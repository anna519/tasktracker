"""
This project helps manage tasks.

To manage tasks library contains three entities: Task, TaskList, User.
User has own list of TaskLists. Each user has at least one standart TaskList "Inbox". Tasks by default saved in this TaskList.
TaskList has priority and list of Tasks. Each Task can exist only in one TaskList. Task has different information about task (priority, date of creation and so on).

Packages:
parser -- the project console app
task_tracker -- the project library
"""