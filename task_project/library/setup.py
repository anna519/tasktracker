from setuptools import setup, find_packages


with open('README.md', 'r') as file:
    long_description = file.read()


setup(
    name="Task_tracker_lib",
    version="1.3",
    description='The library to manage tasks.',
    long_description=long_description,
    author='Hanna Nikitsinskaya',
    url='https://bitbucket.org/anna519/tasktracker',
    packages=find_packages(),
    install_requires = ["jsonpickle"],
    test_suite='lib_task_tracker.tests.tests_runner.get_test_suite',
    include_package_data=True
)