#TaskManager
The application allows you to track tasks, create different task list and
don't forget to do something important.

### Prerequisites
To use the app install jsonpickle:
`pip3 install jsonpicke`

### Getting started

Clone the project from repository:
> git clone https://bitbucket.org/anna519/tasktracker.git

Install the app library from the cloned project folder:
`cd task_project/library`

`python3 setup.py install`

After the installation you can use library 'lib_task_tracker':


`import os`

`import lib_task_tracker.entities.user.user_manager`
 
`import lib_task_tracker.entities.task.task_manager`

`path = os.path.expanduser('~/.TaskTracker/Data')`

`user_manager = lib_task_tracker.entities.user.user_manager.UserManager(storage_path=path)`

`user = user_manager.create_user('email', 'login', 'password')`

`task = task_manager.create_task('Task', user)`

### Running the tests

Run tests from the cloned project folder:
`cd task_tracker/library`

`python3 setup.py test`

### Author
* Hanna Nikitsinskaya
