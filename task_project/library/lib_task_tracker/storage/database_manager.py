""" This module includes DataBaseManager that is generalized class for different databases."""


from lib_task_tracker.exceptions import ObjectNotFoundError
from lib_task_tracker.logger import write_log


class DataBaseManager:
    """ DataBaseManager class performs CRUD actions with the database

    Fields:
        storage -- the object of the storage class
    """

    def __init__(self, storage):

        self.storage = storage

    # region Task functions

    @write_log
    def get_tasks(self):
        """ Return tasks from the database """

        return self.storage.get_tasks()

    @write_log
    def get_task_by_id(self, task_id):
        """ Return task by task_id

        Keyword arguments:
            task_id -- task id to get from the database
        """
        tasks = self.storage.get_tasks()
        for task in tasks:
            if (task.id.hex == task_id or str(task.id) == task_id or
                    task_id == task.id):
                return task

        raise ObjectNotFoundError('Task', 'id', task_id)

    @write_log
    def add_task(self, task):
        """ Add task to the database

        Keyword arguments:
            task -- task to add
        """

        return self.storage.add_task(task)

    @write_log
    def change_task(self, task):
        """ Change task in the database

        Keyword arguments:
            task -- task to change
        """

        return self.storage.change_task(task)

    @write_log
    def remove_task(self, task_id):
        """ Remove task by id from the database

        Keyword arguments:
            task_id -- task id to removed
        """

        return self.storage.remove_task(task_id)

    # endregion

    # region User functions

    @write_log
    def get_users(self):
        """ Return all users from the database """

        return self.storage.get_users()

    @write_log
    def get_user_by_id(self, user_id):
        """ Return user by user_id

        Keyword arguments:
            user_id -- user id to get from the database
        """
        users = self.storage.get_users()
        for user in users:
            if (user.id.hex == user_id or str(user.id) == user_id or
                    user_id == user.id):
                return user

        raise ObjectNotFoundError('User', 'id', user_id)

    @write_log
    def get_user_by_login(self, user_login):
        """ Return user by user_login

        Keyword arguments:
            user_login -- user login to get from the database
        """
        users = self.storage.get_users()
        for user in users:
            if user_login == user.login:
                return user

        raise ObjectNotFoundError('User', 'login', user_login)

    @write_log
    def get_user_by_email(self, user_email):
        """ Return user by user_login

        Keyword arguments:
            user_login -- user login to get from the database
        """
        users = self.storage.get_users()
        for user in users:
            if user_email == user.email:
                return user

        raise ObjectNotFoundError('User', 'email', user_email)

    @write_log
    def add_user(self, user):
        """ Add user to the database

        Keyword arguments:
            user -- user to add
        """

        return self.storage.add_user(user)

    @write_log
    def change_user(self, user):
        """ Change user in the database

        Keyword arguments:
            user -- user to change
        """

        return self.storage.change_user(user)

    @write_log
    def remove_user(self, user_id):
        """ Remove user by id from the database

        Keyword arguments:
            user_id -- user id to removed
        """

        return self.storage.remove_user(user_id)

    # endregion

    # region Task list functions

    @write_log
    def get_task_lists(self):
        """ Returns all task lists from database """

        return self.storage.get_task_lists()

    @write_log
    def get_task_lists_by_id(self, list_id):
        """ Return task list by list_id

        Keyword arguments:
            list_id -- task list id to get from the database
        """

        task_lists = self.storage.get_task_lists()
        for task_list in task_lists:
            if (task_list.id == list_id or str(task_list.id) == list_id or
                    list_id == task_list.id):
                return task_list
        raise ObjectNotFoundError('TaskList', 'id', list_id)

    @write_log
    def add_task_list(self, task_list):
        """ Add task list to the database

        Keyword arguments:
            task_list -- task list to add
        """

        return self.storage.add_task_list(task_list)

    @write_log
    def change_task_list(self, task_list):
        """ Change task list in the database

        Keyword arguments:
            task_list -- task list to change
        """

        return self.storage.change_task_list(task_list)

    @write_log
    def remove_task_list(self, list_id):
        """ Remove task list by id from the json-file

        Keyword arguments:
            list_id -- list id to removed
        """

        return self.storage.remove_task_list(list_id)

    # endregion

