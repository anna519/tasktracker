""" This module includes functions with objects in django app to get library entity."""


from lib_task_tracker.entities.task.task import Task
from lib_task_tracker.entities.task import priority_enum as task_priority_enum, status_enum
from lib_task_tracker.entities.task_list.task_list import TaskList
from lib_task_tracker.entities.task_list import priority_enum as list_priority_enum
from lib_task_tracker.entities.user.user import User


def return_user(web_user):
    auth_user = web_user.auth_user
    user = User(web_user.user_email, auth_user.username, auth_user.password,
                web_user.user_first_name, web_user.user_last_name, web_user.id)
    return user


def return_task_list(web_task_list):
    task_list_priority = list_priority_enum.Priority(web_task_list.task_list_priority)
    task_list = TaskList(web_task_list.task_list_name, None, task_list_priority,
                         id=web_task_list.id)
    task_list.users = [user.auth_user.username for user in web_task_list.task_list_users.all()]

    return task_list


def return_task(web_task):
    task_status = status_enum.Status(web_task.task_status)
    task_priority = task_priority_enum.Priority(web_task.task_priority)

    task = Task(web_task.task_name, web_task.task_list_id, web_task.task_deadline,
                task_priority, web_task.id)
    task.status = task_status
    if web_task.task_parent is not None:
        task.parent = web_task.task_parent.id

    if web_task.depends_on is not None:
        task.is_dependent = True
        task.blocks = [task.id for task in web_task.depends_on.all()]
    return task