""" This package includes class to perform actions with dtabase in django app

    --- DjangoManager ---
    This class performs CRUD actions with User, TaskList, Task objects.


    Modules:
    django_functions -- includes functions to implement CRUD actions with objects in json
    django_manager -- includes functions with objects in django app to get library entity
"""