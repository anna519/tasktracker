""" This module includes class DjangoManager thats performs CRUD actions with the objects"""


from django.contrib.auth.models import User as AuthUser
from lib_task_tracker.storage.django import django_function
from task_tracker.models import Task, TaskList, User

class DjangoManager:
    """This class performs CRUD actions with User, TaskList, Task objects in the django app"""

    # region Functions with tasks
    def get_tasks(self):
        """ Returns all tasks that are stored """

        tasks = []
        for task in Task.objects.all():
            tasks.append(django_function.return_task(task))
        return tasks

    def add_task(self, task):
        """ Add task to the database

        Keyword arguments:
            task -- task to add
        """

        web_task = Task(task.name, task.task_list_id, task.deadline,
                        task.priority, task.id)
        web_task.status = task.status
        web_task.parent = task.parent

        web_task.save()

        return web_task.id

    def change_task(self, task):
        """ Change task in the database

        Keyword arguments:
            task -- task to change
        """

        Task.objects.filter(pk=task.id).update(task_name=task.task_name, task_deadline=task.task_deadline,
                                                      task_status=task.task_status, task_priority=task.task_priority,
                                                      task_parent=task.task_parent)
        task = Task.objects.get(id=task.id)


    def remove_task(self, task_id):
        """ Remove task by id from the database

        Keyword arguments:
            task_id -- task id to removed
        """

        task = Task.objects.get(id=task_id)
        task.delete()
    # endregion

    # region Functions with task lists
    def get_task_lists(self):
        """ Returns all task lists from json-file """

        task_lists = []
        for task_list in TaskList.objects.all():
            task_lists.append(django_function.return_task_list(task_list))

        return task_lists

    def add_task_list(self, task_list):
        """ Add task list to the database

        Keyword arguments:
            task_list -- task list to add
        """

        web_task_list = TaskList(task_list_name=task_list.name,
                                 task_list_priority=task_list.priority.value)
        web_task_list.save()

        for user_login in task_list.users:
            auth_user = AuthUser.objects.get(username=user_login)
            users = User.objects.all()
            user_id = auth_user.id
            user = User.objects.get(id=auth_user.id)
            web_task_list.task_list_users.add(user)

        return web_task_list.id


    def change_task_list(self, task_list):
        """ Change task list in the database

        Keyword arguments:
            task_list -- task list to change
        """

        TaskList.objects.filter(pk=task_list.id).update(name=task_list.name)

        task_list = TaskList.get(id=task_list.id)
        task_list.users.clear()
        for user_id in task_list.users:
            user = User.objects.get(id=user_id)
            task_list.users.add(user)


    def remove_task_list(self, list_id):
        """ Remove task list by id from the json-file

        Keyword arguments:
            list_id -- list id to removed
        """

        task_list = TaskList.objects.get(id=list_id)
        task_list.delete()
    # endregion

    # region Functions with users
    def get_users(self):
        """ Return all users from the database"""

        users = []
        for user in User.objects.all():
            users.append(django_function.return_user(user))
        return users

    def add_user(self, user):
        """ Add user to the database

        Keyword arguments:
            user -- user to add
        """

        auth_user = AuthUser.objects.get(id=user.id)
        web_user = User(auth_user=auth_user, user_first_name=user.first_name,
                               user_last_name=user.last_name, user_email=user.email)
        web_user.save()
        return web_user.id

    def change_user(self, user):
        """ Change user in the database

        Keyword arguments:
            user -- user to change
        """

        user = User.objects.filter(pk=user.id).update(user_first_name=user.first_name,
                                                             user_last_name=user.last_name,
                                                             user_email=user.email)

    def remove_user(self, user_id):
        """ Remove user by id from the database

        Keyword arguments:
            user_id -- user id to removed
        """

        user = User.objects.get(id=user_id)
        user.delete()
    # endregion
