""" This package includes class to perform actions with different kinds of the databases

    --- JsonManager ---
    This class performs CRUD actions with User, TaskList, Task objects.

    --- DjangoManager ---
    This class performs CRUD actions with User, TaskList, Task objects.

    Modules:
    database_manager -- includes DataBaseManager that is generalized class for different databases

    Package:
    django -- includes class to perform actions with dtabase in django app
    json -- includes class to perform actions with JSON-database

"""