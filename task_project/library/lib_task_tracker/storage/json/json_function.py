" This module includes functions to implement CRUD actions with objects in json."


import jsonpickle
import os.path


def get_objects_from_json_file(file_path):
    """ Return objects from the json-file

    Keyword arguments:
        file_path -- file from which object is get to
    """

    if os.path.isfile(file_path):
        with open(file_path, 'r') as file:
            data = jsonpickle.decode(file.read())
    else:
        data = []
    return data


def save_data_to_json_file(file_path, data):

    """ Save data to json-file

    Keyword arguments:
        file_path -- file into which the data is saved
        data -- data that is saved
    """

    with open(file_path, 'w') as file:
        file.write(jsonpickle.encode(data))


def add_object_to_json_file(file_path, obj):
    """ Add object to the json-file

    Keyword arguments:
        file_path -- file to which object is added
        obj -- object that is added
    """

    data = get_objects_from_json_file(file_path)

    data.append(obj)

    save_data_to_json_file(file_path, data)


def change_object_in_json_file(file_path, obj):
    """ Change object in the json-file

    Keyword arguments:
        file_path -- file in which object is changed
        obj -- object that is added
    """

    data = get_objects_from_json_file(file_path)

    found = False

    for index in range(len(data)):
        if data[index].id == obj.id:
            data[index] = obj
            found = True

    if found:
        save_data_to_json_file(file_path, data)


def remove_object_from_json_file(file_path, obj_id):
    """ Remove object from the json-file

    Keyword arguments:
        file_path -- file from which object is removed
        obj_id -- object id that is removed
    """

    data = get_objects_from_json_file(file_path)

    if len(data) != 0:
        found = False

        for element in data:
            if element.id == obj_id:
                data.remove(element)
                found = True

        if found:
            save_data_to_json_file(file_path, data)