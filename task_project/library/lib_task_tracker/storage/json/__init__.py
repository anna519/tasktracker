""" This package includes class to perform actions with JSON-database

    --- JsonManager ---
    This class performs CRUD actions with User, TaskList, Task objects.


    Modules:
    json_functions -- includes functions to implement CRUD actions with objects in json
    json_manager -- includes class JsonManager thats performs CRUD actions with the objects

"""