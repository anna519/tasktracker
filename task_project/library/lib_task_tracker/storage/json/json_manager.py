""" This module includes class JsonManager thats performs CRUD actions with the objects."""


import os
from datetime import datetime
from lib_task_tracker.storage.json import json_function
from lib_task_tracker.exceptions import PathNotFoundError
from lib_task_tracker.entities.task.status_enum import Status


class JsonManager:
    """ This class performs CRUD actions with User, TaskList, Task objects.

    Fields:
        storage_path -- path where the data is stored
        tasks_file -- path where the data about tasks is stored
        lists_file -- path where the data about lists is stored
        users_file -- path where the data about users is stored
    """
    def __init__(self, storage_path):
        """ Initialize the JsonManager object

        Keyword arguments:
            storage_path -- path where the data will be stored

        Raises:
            PathNotFoundError -- if passed path is not found
        """

        if not os.path.exists(storage_path):
            raise PathNotFoundError(storage_path)

        self.storage_path = storage_path

        self.tasks_file = os.path.join(self.storage_path, 'tasks.json')
        self.lists_file = os.path.join(self.storage_path, 'lists.json')
        self.users_file = os.path.join(self.storage_path, 'users.json')


    # region Functions with tasks
    def get_tasks(self):
        """ Returns all tasks from json-file """

        tasks = json_function.get_objects_from_json_file(self.tasks_file)
        for task in tasks:
            if task.deadline is not None and task.status.value <= 1 and task.deadline < datetime.now():
                task.status = Status.FAILED

        return tasks

    def add_task(self, task):
        """ Add task to the json-file

        Keyword arguments:
            task -- task to add to the json-file
        """

        json_function.add_object_to_json_file(self.tasks_file, task)

    def change_task(self, task):
        """ Change task in the json-file

        Keyword arguments:
            task -- task to change in the json-file
        """

        json_function.change_object_in_json_file(self.tasks_file, task)

    def remove_task(self, task_id):
        """ Remove task by id from the json-file

        Keyword arguments:
            task_id -- task id to removed
        """

        json_function.remove_object_from_json_file(self.tasks_file, task_id)

    # endregion

    # region Functions with task lists

    def get_task_lists(self):
        """ Returns all task lists from json-file """

        lists = json_function.get_objects_from_json_file(self.lists_file)
        return lists

    def add_task_list(self, task_list):
        """ Add task list to the json-file

        Keyword arguments:
            task_list -- task list to add to the json-file
        """

        json_function.add_object_to_json_file(self.lists_file, task_list)

    def change_task_list(self, task_list):
        """ Change task list in the json-file

        Keyword arguments:
            task_list -- task list to change in the json-file
        """

        json_function.change_object_in_json_file(self.lists_file, task_list)

    def remove_task_list(self, list_id):
        """ Remove task list by id from the json-file

        Keyword arguments:
            list_id -- list id to removed
        """

        json_function.remove_object_from_json_file(self.lists_file, list_id)

    # endregion

    # region Functions with users

    def get_users(self):
        """ Return all users from storage"""

        users = json_function.get_objects_from_json_file(self.users_file)
        return users

    def add_user(self, user):
        """ Add user to the json-file

        Keyword arguments:
            user -- user to add to the json-file
        """

        json_function.add_object_to_json_file(self.users_file, user)

    def change_user(self, user):
        """ Change user in the json-file

        Keyword arguments:
            user -- user to change in the json-file
        """

        json_function.change_object_in_json_file(self.users_file, user)

    def remove_user(self, user_id):
        """ Remove user by id from the json-file

        Keyword arguments:
            user_id -- user id to removed
        """

        json_function.remove_object_from_json_file(self.users_file, user_id)

    # endregion