""" This module includes the app exceptions """


class NotExistingObjectError(Exception):
    """ Raises when the object doesn't exist in database"""

    def __init__(self, object_name):
        super().__init__("The object {} doesn't exists.".format(object_name))


class ObjectNotFoundError(Exception):
    """ Raises when the object wasn't found"""

    def __init__(self, object_name, object_atr, object_atr_value):
        super().__init__("The {} object with {}: {} wasn't found".
                         format(object_name, object_atr, object_atr_value))


class ExistingUserByLoginError(Exception):
    """ Raises when the user with such login already exists in database"""

    def __init__(self, login):
        super().__init__(
            'The user with login:{} already exists.'.format(login))


class ExistingUserByEmailError(Exception):
    """ Raises when the user with such email already exists in database"""

    def __init__(self, email):
        super().__init__(
            'The user with email:{} already exists.'.format(email))


class ExistingTaskListByNameError(Exception):
    """ Raises when the user already has list with such name in database"""

    def __init__(self, task_list_name):
        super().__init__(
            'The user already has task list:{}.'.format(task_list_name))


class NotValidParameterError(Exception):
    """ Raises when the written parameter isn't valid"""

    def __init__(self, value, param_name):
        super().__init__("{} isn't valid {}".format(value, param_name))


class ExistingObjectInListError(Exception):
    """ Raises when the object already exists in the list"""

    def __init__(self, object_name, object_list):
        self.object_name = object_name
        super().__init__(
            'The {} is already in list:{}.'.format(object_name, object_list))


class NotExistingObjectInListError(Exception):
    """ Raises when the object doesn't exist in the list"""

    def __init__(self, object_name, object_list):
        self.object_name = object_name
        super().__init__(
            "The {} isn't in list:{}.".format(object_name, object_list))


class TaskListWithOneUserError(Exception):
    """ Raises when the last user is getting removed from task list"""

    def __init__(self, user_login, task_list_name):
        super().__init__(
            "Can't remove only {} from {}".format(user_login, task_list_name)
        )


class StandartTaskListError(Exception):
    """ Raises when the user try to change standart task list"""

    def __init__(self, task_list_name):
        super().__init__(
            "Task list {} is standart".format(task_list_name)
        )


class PathNotFoundError(Exception):
    """ Raises if the path hasn't been found"""

    def __init__(self, path):
        super().__init__("The path {} doesn't exist.".format(path))


class SettingsNotFoundError(Exception):
    """ Raises if the settings file hasn't been found"""

    def __init__(self):
        super().__init__("The settings file hasn't been found.")


class LoggerConfigNotFoundError(Exception):
    """ Raises if the logger config file hasn't been found"""

    def __init__(self):
        super().__init__("The logger config file hasn't been found.")


class TaskIsBlockedError(Exception):
    """ Raises if the task is blocked to change status"""

    def __init__(self):
        super().__init__("The task is blocked to change status.")
