""" This package includes unittests that cover all app functions

    Modules:
    test_database_manager -- includes unittests to test DataBaseManager functions
    test_list_manager -- includes unittests to test ListManager functions
    test_task_manager -- includes unittests to test TaskManager functions
    test_user_manager -- includes unittests to test UserManager functions
    tests_runner -- includes function that creates TestSuite to run all unittests at the same time
"""