""" This module includes unittests to test DataBaseManager functions"""


import unittest
import os
import shutil
from lib_task_tracker.storage.json.json_manager import JsonManager
from lib_task_tracker.storage.database_manager import DataBaseManager
from lib_task_tracker.entities.user.user import User
from lib_task_tracker.entities.task_list.task_list import TaskList
from lib_task_tracker.entities.task.task import Task
from lib_task_tracker.exceptions import ObjectNotFoundError
from lib_task_tracker.logger import disable_logger, enable_logger


class TestDatabaseManager(unittest.TestCase):
    def setUp(self):
        disable_logger()

        # Create dir to store tests data

        self.temp_dir = os.path.join(os.path.abspath(os.curdir), 'temp_dir')
        if os.path.exists(self.temp_dir):
            shutil.rmtree(self.temp_dir, ignore_errors=True)
        os.mkdir(self.temp_dir)

        storage = JsonManager(self.temp_dir)
        self.storage = DataBaseManager(storage)

        self.user = User('email', 'login', 'password', 'first_name', 'last_name')
        self.list = TaskList('TaskList', 'login')
        self.task = Task('Task', self.list.id)

        self.user_number = len(self.storage.get_users())
        self.list_number = len(self.storage.get_task_lists())
        self.task_number = len(self.storage.get_tasks())

    def test_add_user(self):
        self.storage.add_user(self.user)
        new_user_number = len(self.storage.get_users())

        self.assertEqual(self.user_number + 1, new_user_number)
        self.storage.remove_user(self.user.id)

    def test_delete_user(self):
        self.storage.add_user(self.user)
        new_user_number = len(self.storage.get_users())

        self.assertEqual(self.user_number + 1, new_user_number)

        self.storage.remove_user(self.user.id)

        user_number_after_removing = len(self.storage.get_users())
        self.assertEqual(new_user_number - 1, user_number_after_removing)

    def test_change_user_login(self):
        self.storage.add_user(self.user)
        self.user.login = 'new_login'
        self.storage.change_user(self.user)

        user = self.storage.get_user_by_id(self.user.id)

        self.assertEqual(user.login, 'new_login')
        self.storage.remove_user(self.user.id)

    def test_get_user_by_id(self):
        user_id = self.user.id
        self.storage.add_user(self.user)

        user = self.storage.get_user_by_id(user_id)
        self.assertIsNotNone(user)

        self.storage.remove_user(user_id)

    def test_get_user_by_login(self):
        user_login = self.user.login
        self.storage.add_user(self.user)

        user = self.storage.get_user_by_login(user_login)
        self.assertIsNotNone(user)

        self.storage.remove_user(self.user.id)

    def test_get_user_by_email(self):
        user_email = self.user.email
        self.storage.add_user(self.user)

        user = self.storage.get_user_by_email(user_email)
        self.assertIsNotNone(user)

        self.storage.remove_user(self.user.id)

    def test_get_not_existing_user(self):
        self.assertRaises(ObjectNotFoundError, self.storage.get_user_by_id,
                          'not_existing_id')
        self.assertRaises(ObjectNotFoundError, self.storage.get_user_by_login,
                          'not_existing_login')
        self.assertRaises(ObjectNotFoundError, self.storage.get_user_by_email,
                          'not_existing_email')

    def test_add_task_list(self):
        self.storage.add_task_list(self.list)
        new_list_number = len(self.storage.get_task_lists())

        self.assertEqual(self.list_number + 1, new_list_number)
        self.storage.remove_task_list(self.list.id)

    def test_delete_task_list(self):
        self.storage.add_task_list(self.list)
        new_list_number = len(self.storage.get_task_lists())

        self.assertEqual(self.list_number + 1, new_list_number)

        self.storage.remove_task_list(self.list.id)

        list_number_after_removing = len(self.storage.get_task_lists())
        self.assertEqual(new_list_number - 1, list_number_after_removing)

    def test_change_task_list_name(self):
        self.storage.add_task_list(self.list)
        self.list.name = 'new_name'
        self.storage.change_task_list(self.list)

        list = self.storage.get_task_lists_by_id(self.list.id)

        self.assertEqual(list.name, 'new_name')
        self.storage.remove_task_list(self.list.id)

    def test_get_task_list_by_id(self):
        list_id = self.list.id
        self.storage.add_task_list(self.list)

        list = self.storage.get_task_lists_by_id(list_id)
        self.assertIsNotNone(list)

        self.storage.remove_task_list(list_id)

    def test_get_not_existing_list(self):
        self.assertRaises(ObjectNotFoundError, self.storage.get_task_lists_by_id,
                          'not_existing_task_list_id')

    def test_add_task(self):
        self.storage.add_task(self.task)
        new_task_number = len(self.storage.get_tasks())

        self.assertEqual(self.task_number + 1, new_task_number)
        self.storage.remove_task(self.task.id)

    def test_delete_task(self):
        self.storage.add_task(self.task)
        new_task_number = len(self.storage.get_tasks())

        self.assertEqual(self.task_number + 1, new_task_number)

        self.storage.remove_task(self.task.id)

        task_number_after_removing = len(self.storage.get_tasks())
        self.assertEqual(new_task_number - 1, task_number_after_removing)

    def test_change_task_name(self):
        self.storage.add_task(self.task)
        self.task.name = 'new_name'
        self.storage.change_task(self.task)

        task = self.storage.get_task_by_id(self.task.id)

        self.assertEqual(task.name, 'new_name')
        self.storage.remove_task(self.task.id)

    def test_get_task_by_id(self):
        task_id = self.task.id
        self.storage.add_task(self.task)

        task = self.storage.get_task_by_id(task_id)
        self.assertIsNotNone(task)

        self.storage.remove_task(task_id)

    def test_get_not_existing_task(self):
        self.assertRaises(ObjectNotFoundError,
                          self.storage.get_task_by_id,
                          'not_existing_task_id')

    def tearDown(self):
        shutil.rmtree(self.temp_dir, ignore_errors=True)
        enable_logger()

if __name__ == '__main__':
    unittest.main()
