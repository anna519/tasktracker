""" This module includes unittests to test ListManager functions"""


import os
import unittest
import shutil
from lib_task_tracker.storage.json.json_manager import JsonManager
from lib_task_tracker.storage.database_manager import DataBaseManager
from lib_task_tracker import exceptions
from lib_task_tracker.entities.user.user_manager import UserManager
from lib_task_tracker.entities.task_list.list_manager import ListManager
from lib_task_tracker.entities.task.task_manager import TaskManager
from lib_task_tracker.entities.task_list.priority_enum import Priority
from lib_task_tracker.logger import enable_logger, disable_logger


class TestListFunctions(unittest.TestCase):

    def setUp(self):
        disable_logger()

        # Create dir to store tests data

        self.temp_dir = os.path.join(os.path.abspath(os.curdir), 'temp_dir')
        if os.path.exists(self.temp_dir):
            shutil.rmtree(self.temp_dir, ignore_errors=True)
        os.mkdir(self.temp_dir)

        self.storage = DataBaseManager(JsonManager(self.temp_dir))

        self.user_mng = UserManager(self.storage)
        self.list_mng = ListManager(self.storage)
        self.task_mng = TaskManager(self.storage)

    # region test create list
    def test_create_unique_list(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        task_list = self.list_mng.create_list("TaskList", user)
        task_list_id = task_list.id
        task_list = self.list_mng.storage.get_task_lists_by_id(task_list_id)

        self.assertIsNotNone(task_list)

        self.user_mng.remove_user(user)

    def test_create_not_unique_list(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        self.assertRaises(exceptions.ExistingTaskListByNameError,
                          self.list_mng.create_list, "Inbox", user)

        self.user_mng.remove_user(user)

    def test_create_unique_list_with_priority(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        task_list = self.list_mng.create_list("TaskList", user, priority="low")
        task_list_id = task_list.id
        task_list = self.list_mng.storage.get_task_lists_by_id(task_list_id)

        self.assertIsNotNone(task_list)
        self.assertEqual(task_list.priority, Priority.LOW)

        self.user_mng.remove_user(user)

    def test_change_task_list_name(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        task_list = self.list_mng.create_list("TaskList", user)
        self.list_mng.change_task_list(task_list, user, name="TaskList_1")

        self.assertEqual(task_list.name, "TaskList_1")

        self.user_mng.remove_user(user)
    # endregion

    # region test change list
    def test_change_task_list_name_to_existing(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        task_list = self.list_mng.create_list("TaskList", user)
        self.assertRaises(exceptions.ExistingTaskListByNameError,
                          self.list_mng.change_task_list, task_list,
                          user, name="Inbox")

        self.user_mng.remove_user(user)

    def test_change_task_list_priority(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        task_list = self.list_mng.create_list("TaskList", user)
        self.list_mng.change_task_list(task_list, user, priority="high")

        self.assertEqual(task_list.priority, Priority.HIGH)

        self.user_mng.remove_user(user)

    def test_change_task_list_priority_to_incorrect(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        task_list = self.list_mng.create_list("TaskList", user)
        self.assertRaises(exceptions.NotValidParameterError,
                          self.list_mng.change_task_list, task_list, user,
                          priority="5")

        self.user_mng.remove_user(user)
    # endregion

    # test remove list
    def test_remove_task_list(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        task_list = self.list_mng.create_list("TaskList", user)
        task_list_id = task_list.id

        self.user_mng.remove_user(user)

        self.assertRaises(exceptions.ObjectNotFoundError,
                          self.list_mng.storage.get_task_lists_by_id, task_list_id)

    def test_remove_task_list_with_tasks(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        task = self.task_mng.create_task("Task", user)
        task_id = task.id
        self.user_mng.remove_user(user)

        self.assertRaises(exceptions.ObjectNotFoundError,
                          self.list_mng.storage.get_task_by_id, task_id)

    def test_add_user_to_task_list(self):
        user_1 = self.user_mng.create_user("email", "login", "password",
                                           "first_name", "second_name")

        user_2 = self.user_mng.create_user("email_2", "login_2", "password",
                                           "first_name", "second_name")
        task_list = self.list_mng.create_list("TaskList", user_1)
        self.list_mng.add_user_to_list(task_list, user_1, user_2.login)

        self.assertIn(user_2.login, task_list.users)

        self.user_mng.remove_user(user_1)
        self.user_mng.remove_user(user_2)

    def test_add_existing_user_to_task_list(self):
        user_1 = self.user_mng.create_user("email", "login", "password",
                                           "first_name", "second_name")

        task_list = self.list_mng.create_list("TaskList", user_1)
        self.assertRaises(exceptions.ExistingObjectInListError,
                          self.list_mng.add_user_to_list, task_list,
                          user_1, user_1.login)

        self.user_mng.remove_user(user_1)

    def test_remove_user_from_task_list(self):
        user_1 = self.user_mng.create_user("email", "login", "password",
                                           "first_name", "second_name")

        user_2 = self.user_mng.create_user("email_2", "login_2", "password",
                                           "first_name", "second_name")
        task_list = self.list_mng.create_list("TaskList", user_1)
        self.list_mng.add_user_to_list(task_list, user_1, user_2.login)

        self.assertIn(user_2.login, task_list.users)

        self.list_mng.remove_user_from_list(task_list, user_2, user_1.login)

        self.assertNotIn(user_1.login, task_list.users)

        self.user_mng.remove_user(user_1)
        self.user_mng.remove_user(user_2)

    def test_remove_not_existing_user_from_task_list(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        task_list = self.list_mng.create_list("TaskList", user)
        self.assertRaises(exceptions.NotExistingObjectInListError,
                          self.list_mng.remove_user_from_list, task_list,
                          user, "not_existing_login")

        self.user_mng.remove_user(user)
    # endregion

    def tearDown(self):
        shutil.rmtree(self.temp_dir, ignore_errors=True)
        enable_logger()

if __name__ == '__main__':
    unittest.main()
