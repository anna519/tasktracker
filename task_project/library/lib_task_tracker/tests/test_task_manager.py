""" This module includes unittests to test TaskManager functions"""


import os
import unittest
import shutil
from lib_task_tracker.storage.json.json_manager import JsonManager
from lib_task_tracker.storage.database_manager import DataBaseManager
from lib_task_tracker import exceptions
from lib_task_tracker.entities.user.user_manager import UserManager
from lib_task_tracker.entities.task_list.list_manager import ListManager
from lib_task_tracker.entities.task.task_manager import TaskManager
from lib_task_tracker.entities.task.priority_enum import Priority
from lib_task_tracker.entities.task.status_enum import Status
from lib_task_tracker.logger import disable_logger, enable_logger


class TestTaskFunctions(unittest.TestCase):

    def setUp(self):
        disable_logger()

        # Create dir to store tests data

        self.temp_dir = os.path.join(os.path.abspath(os.curdir), 'temp_dir')
        if os.path.exists(self.temp_dir):
            shutil.rmtree(self.temp_dir, ignore_errors=True)
        os.mkdir(self.temp_dir)

        self.storage = DataBaseManager(JsonManager(self.temp_dir))

        self.user_mng = UserManager(self.storage)
        self.list_mng = ListManager(self.storage)
        self.task_mng = TaskManager(self.storage)


    # region test create task
    def test_create_task(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        task = self.task_mng.create_task("Task", user)
        task_id = task.id
        task = self.list_mng.storage.get_task_by_id(task_id)

        self.assertIsNotNone(task)

        self.user_mng.remove_user(user)

    def test_create_task_in_not_existing_list(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        self.assertRaises(TypeError, self.task_mng.create_task,
                          "Task", user, "not_existing_list")

        self.user_mng.remove_user(user)

    def test_create_task_with_deadline(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        task = self.task_mng.create_task("Task", user, deadline="30-08-2018")
        task_id = task.id
        task = self.list_mng.storage.get_task_by_id(task_id)

        self.assertIsNotNone(task)

        self.user_mng.remove_user(user)

    def test_create_task_with_incorrect_deadline(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        self.assertRaises(exceptions.NotValidParameterError,
                          self.task_mng.create_task,
                          "Task", user, deadline="3008")

        self.user_mng.remove_user(user)

    def test_create_task_with_priority(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        task = self.task_mng.create_task("Task", user, priority="low")
        task_id = task.id
        task = self.list_mng.storage.get_task_by_id(task_id)

        self.assertIsNotNone(task)
        self.assertEqual(task.priority, Priority.LOW)

        self.user_mng.remove_user(user)

    def test_create_task_with_incorrect_priority(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        self.assertRaises(exceptions.NotValidParameterError,
                          self.task_mng.create_task,
                          "Task", user, priority="5")

        self.user_mng.remove_user(user)
    # endregion

    # region test change task
    def test_change_task_name(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        task = self.task_mng.create_task("Task", user)
        self.task_mng.change_task(task, user, name="new_task_name")

        self.assertEqual(task.name, "new_task_name")

        self.user_mng.remove_user(user)

    def test_change_task_priority(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        task = self.task_mng.create_task("Task", user, priority="high")

        self.assertEqual(task.priority, Priority.HIGH)

        self.task_mng.change_task(task, user, priority="low")

        self.assertEqual(task.priority, Priority.LOW)

        self.user_mng.remove_user(user)

    def test_change_task_priority_to_incorrect(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        task = self.task_mng.create_task("Task", user, priority="high")

        self.assertEqual(task.priority, Priority.HIGH)

        self.assertRaises(exceptions.NotValidParameterError,
                          self.task_mng.change_task, task, user, priority="5")

        self.user_mng.remove_user(user)

    def test_change_task_status(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        task = self.task_mng.create_task("Task", user)

        self.task_mng.change_task(task, user, status="done")

        self.assertEqual(task.status, Status.DONE)

        self.user_mng.remove_user(user)

    def test_change_task_status_to_incorrect(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        task = self.task_mng.create_task("Task", user, priority="high")

        self.assertRaises(exceptions.NotValidParameterError,
                          self.task_mng.change_task, task, user, status="5")

        self.user_mng.remove_user(user)

    def test_change_task_deadline(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        task = self.task_mng.create_task("Task", user)

        self.task_mng.change_task(task, user, deadline="30-08-2018")

        self.assertEqual(task.deadline.strftime("%H-%M %d-%m-%Y"),
                         "00-00 30-08-2018")

        self.user_mng.remove_user(user)

    def test_change_task_deadline_ti_incorrect(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        task = self.task_mng.create_task("Task", user)

        self.assertRaises(exceptions.NotValidParameterError,
                         self.task_mng.change_task, task, user,
                          deadline="3008")

        self.user_mng.remove_user(user)
    # endregion

    # region Task link test

    def test_add_subtask(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        task_1 = self.task_mng.create_task("Task", user)
        is_added = self.task_mng.add_sub_task(task_1, user, "Subtask")
        self.assertEqual(is_added, True)

        self.user_mng.remove_user(user)


    def test_delete_subtask(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        task_1 = self.task_mng.create_task("Task", user)
        self.task_mng.add_sub_task(task_1, user, "Subtask")
        task_subtask = self.task_mng.get_task_subtasks(task_1.id)
        self.assertIsNotNone(task_subtask)

        is_deleted = self.task_mng.remove_subtask(task_1, user, task_subtask[0])
        self.assertEqual(is_deleted, True)

        self.user_mng.remove_user(user)

    def test_add_dependent_task(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")
        task_1 = self.task_mng.create_task("Task", user)
        self.task_mng.add_dependent_task(task_1, user, "Subtask")

        dependent_task = task_1.blocks[0]
        dependent_task = self.task_mng.storage.get_task_by_id(dependent_task)

        blocking_task = self.task_mng.get_blocking_task(dependent_task)
        self.assertEqual(blocking_task.id, task_1.id)

        self.user_mng.remove_user(user)

    def test_delete_dependent_task(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        task_1 = self.task_mng.create_task("Task", user)
        self.task_mng.add_dependent_task(task_1, user, "Subtask")

        dependent_task = task_1.blocks[0]
        dependent_task = self.task_mng.storage.get_task_by_id(dependent_task)

        is_deleted = self.task_mng.remove_dependent_task(task_1, user, dependent_task)
        self.assertEqual(is_deleted, True)
        self.user_mng.remove_user(user)
    # endregion

    def test_remove_task(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        task = self.task_mng.create_task("Task", user)
        task_id = task.id
        self.task_mng.remove_task(task, user)

        self.assertRaises(exceptions.ObjectNotFoundError,
                          self.task_mng.storage.get_task_by_id, task_id)
        self.user_mng.remove_user(user)

    def test_remove_not_existing_task(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "second_name")

        self.assertRaises(TypeError, self.task_mng.remove_task,
                          "not_existing_task", user)

        self.user_mng.remove_user(user)

    def tearDown(self):
        shutil.rmtree(self.temp_dir, ignore_errors=True)
        enable_logger()


if __name__ == '__main__':
    unittest.main()
