""" This module includes unittests to test UserManager functions"""


import os
import unittest
import shutil
from lib_task_tracker.storage.json.json_manager import JsonManager
from lib_task_tracker.storage.database_manager import DataBaseManager
from lib_task_tracker import exceptions
from lib_task_tracker.entities.user.user_manager import UserManager
from lib_task_tracker.entities.task_list.list_manager import ListManager
from lib_task_tracker.logger import disable_logger, enable_logger


class TestUserFunctions(unittest.TestCase):

    def setUp(self):
        disable_logger()

        # Create dir to store tests data

        self.temp_dir = os.path.join(os.path.abspath(os.curdir), 'temp_dir')
        if os.path.exists(self.temp_dir):
            shutil.rmtree(self.temp_dir, ignore_errors=True)
        os.mkdir(self.temp_dir)

        self.storage = DataBaseManager(JsonManager(self.temp_dir))

        self.user_mng = UserManager(self.storage)
        self.list_mng = ListManager(self.storage)

    def test_create_unique_user(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "last_name")
        find_user = self.user_mng.storage.get_user_by_id(user.id)

        self.assertIsNotNone(find_user)
        self.assertEqual(user.login, "login")
        self.assertEqual(user.email, "email")

        self.user_mng.remove_user(user)

    def test_create_user_with_not_unique_email(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "last_name")

        self.assertRaises(exceptions.ExistingUserByEmailError,
                          self.user_mng.create_user, "email", "login_1",
                          "password", "first_name", "last_name")

        self.user_mng.remove_user(user)

    def test_create_user_with_not_unique_login(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "last_name")

        self.assertRaises(exceptions.ExistingUserByLoginError,
                          self.user_mng.create_user, "email_1", "login",
                          "password", "first_name", "last_name")

        self.user_mng.remove_user(user)

    def test_change_user_email_to_unique_email(self):
        user_1 = self.user_mng.create_user("email", "login", "password",
                                           "first_name", "last_name")

        self.user_mng.change_user(user_1, email="new_email")

        self.assertEqual(user_1.email, "new_email")

        self.user_mng.remove_user(user_1)

    def test_change_user_email_to_existing_email(self):
        user_1 = self.user_mng.create_user("email", "login", "password",
                                           "first_name", "last_name")

        user_2 = self.user_mng.create_user("email_1", "login_1", "password",
                                           "first_name", "last_name")

        self.assertRaises(exceptions.ExistingUserByEmailError,
                          self.user_mng.change_user, user_1,
                          email="email_1")

        self.user_mng.remove_user(user_1)
        self.user_mng.remove_user(user_2)

    def test_change_user_login_to_unique_login(self):
        user_1 = self.user_mng.create_user("email", "login", "password",
                                           "first_name", "last_name")

        self.user_mng.change_user(user_1, login="new_login")

        self.assertEqual(user_1.login, "new_login")

        self.user_mng.remove_user(user_1)

    def test_change_user_login_to_existing_login(self):
        user_1 = self.user_mng.create_user("email", "login", "password",
                                           "first_name", "last_name")

        user_2 = self.user_mng.create_user("email_1", "login_1", "password",
                                           "first_name", "last_name")

        self.assertRaises(exceptions.ExistingUserByLoginError,
                          self.user_mng.change_user, user_1,
                          login="login_1")

        self.user_mng.remove_user(user_1)
        self.user_mng.remove_user(user_2)


    def test_change_user_password(self):
        user_1 = self.user_mng.create_user("email", "login", "password",
                                           "first_name", "last_name")

        self.user_mng.change_user(user_1, password="new_password")

        self.assertEqual(user_1.password, "new_password")

        self.user_mng.remove_user(user_1)

    def test_change_user_first_name(self):
        user_1 = self.user_mng.create_user("email", "login", "password",
                                           "first_name", "last_name")

        self.user_mng.change_user(user_1, first_name="new_first_name")

        self.assertEqual(user_1.first_name, "new_first_name")

        self.user_mng.remove_user(user_1)

    def test_change_user_last_name(self):
        user_1 = self.user_mng.create_user("email", "login", "password",
                                           "first_name", "last_name")

        self.user_mng.change_user(user_1, last_name="new_last_name")

        self.assertEqual(user_1.last_name, "new_last_name")

        self.user_mng.remove_user(user_1)

    def test_remove_user(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "last_name")
        user_id = user.id
        self.user_mng.remove_user(user)

        self.assertRaises(exceptions.ObjectNotFoundError,
                          self.user_mng.storage.get_user_by_id,user_id)

    def test_remove_user_with_task_lists(self):
        user = self.user_mng.create_user("email", "login", "password",
                                         "first_name", "last_name")

        task_list = self.list_mng.create_list("Qwerty", user)
        task_list_id = task_list.id

        self.user_mng.remove_user(user)
        self.assertRaises(exceptions.ObjectNotFoundError,
                          self.user_mng.storage.get_task_lists_by_id, task_list_id)

    def tearDown(self):
        shutil.rmtree(self.temp_dir, ignore_errors=True)
        enable_logger()

if __name__ == '__main__':
    unittest.main()
