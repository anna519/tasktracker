""" This module includes function that creates TestSuite to run all unittests at the same time."""


import unittest
from lib_task_tracker.tests.test_database_manager import TestDatabaseManager
from lib_task_tracker.tests.test_user_manager import TestUserFunctions
from lib_task_tracker.tests.test_list_manager import TestListFunctions
from lib_task_tracker.tests.test_task_manager import TestTaskFunctions


def get_test_suite():
    suite = unittest.TestSuite()
    suite.addTests(unittest.makeSuite(TestDatabaseManager))
    suite.addTests(unittest.makeSuite(TestUserFunctions))
    suite.addTests(unittest.makeSuite(TestListFunctions))
    suite.addTests(unittest.makeSuite(TestTaskFunctions))

    return suite