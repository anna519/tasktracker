""" This module includes the decorator that writes debug and error logs. the functions
to get and manage the app logger"""


import logging
from functools import wraps

from lib_task_tracker import exceptions

APP_NAME = 'taskTracker'


def get_logger():
    """ Return app logger """

    logger = logging.getLogger(APP_NAME)

    return logger


def write_log(func):
    """ Decorator that writes when the task was started and finished

    Keyword argument:
        func -- function to decorated
    """

    @wraps(func)
    def wrap_log(*args, **kwargs):
        logger = logging.getLogger(APP_NAME)

        logger.debug("{} function started".format(func.__name__))

        try:
            result = func(*args, **kwargs)
        except exceptions.SettingsNotFoundError as e:
            logger.critical(e)
            logger.debug("The {} function stopped after the error".format(func.__name__))
            raise
        except Exception as e:
            logger.error(e)
            logger.debug("The {} function stopped after the error".format(func.__name__))
            raise
        logger.debug("The {} function is finished".format(func.__name__))
        return result
    return wrap_log


def disable_logger():
    """ Disable the app logger"""

    logging.getLogger(APP_NAME).disabled = True


def enable_logger():
    """ Enable the app logger"""

    logging.getLogger(APP_NAME).disabled = False



