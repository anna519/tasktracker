""" This package includes classes that represent the main logic for User.

    --- User ---
    User entity is a user who can has tasklists and complete tasks. User necessery has
    unique email, unique login and password. He also has first name and last name.

    --- UserManager ---
    UserManager is a class that helps manage User entity with database.
    It helps create, update, remove user.


Modules:
    user_manager -- includes the class that represents UserManager
    user -- includes the class that represents the User entity
"""