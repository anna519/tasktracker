""" This module includes functions for the User entity
UserManager is a class that helps manage User entity with database.
It helps create, update, remove user.

Example:
    >>> import os
    >>> import lib_task_tracker.entities.user.user_manager


    >>> path = os.path.expanduser('~/.TaskTracker/Data')
    >>> user_manager = lib_task_tracker.entities.user.user_manager.UserManager(storage_path=path)
    >>> user = user_manager.create_user('email', 'login', 'password')

The functions that can be performed with the user:
create_user, change_user, remove_user, get_user_lists, get_user_tasks,
get_user_unfinished_tasks, get_user_finished_tasks, show_full_user_info,
show_short_user_info, show_user_tasks

"""
from lib_task_tracker.storage.database_manager import DataBaseManager
from lib_task_tracker.storage.json.json_manager import JsonManager
from lib_task_tracker.entities.task_list import list_manager
from lib_task_tracker.entities.user.user import User
from lib_task_tracker import exceptions
from lib_task_tracker.logger import get_logger, write_log


class UserManager:
    """ Performs actions for work with User

    Fields:
    storage -- DataBaseManager object that used to work with data
    logger -- the app logger
    """

    def __init__(self, storage=None, storage_path=None):
        """ Initialize the object

        Keyword arguments:
            storage -- the storage to save data
            storage_path -- the path where the data store
            logger -- app logger
        """

        self.logger = get_logger()

        if storage_path is None and storage is None:
            raise exceptions.NotValidParameterError(storage, 'storage')

        if storage and not isinstance(storage, DataBaseManager):
            raise TypeError("The storage isn't of the type DataBaseManager")

        if storage:
            self.storage = storage
            return

        self.storage = DataBaseManager(JsonManager(storage_path))

    @write_log
    def create_user(self, email, login, password, first_name=None,
                    last_name=None, id=None):
        """ Create a user object with "Inbox" task list and add it
        to a database

        Keyword arguments:
            email -- user email
            login -- user login
            password -- user password
            first_name -- user first name
            second_name -- user second name

        Raises:
            ExistingUserByEmailError -- if user with such email already exist
            ExistingUserByLoginError -- if user with such login already exist
        """

        users = self.storage.get_users()

        # If user with such email already exist, raise error
        if any(user.email == email for user in users):
            raise exceptions.ExistingUserByEmailError(email)

        # If user with such login already exist
        elif any(user.login == login for user in users):
            raise exceptions.ExistingUserByLoginError(login)

        if first_name is None:
            first_name = ""
        if last_name is None:
            last_name = ""

        # Create user
        new_user = User(email, login, password, first_name, last_name, id)
        list_mng = list_manager.ListManager(self.storage)

        # Create standart task list
        self.storage.add_user(new_user)
        list_mng.create_list("Inbox", new_user, is_standard=True)

        self.logger.info("The user with id: {} was added.".format(new_user.id))
        return new_user

    @write_log
    def remove_user(self, user):
        """ Remove user from the database

        Keyword arguments:
            user -- user to remove from the database

        Raises:
            NotExistingObjectError -- if user is None
        """

        if user is None:
            raise exceptions.NotExistingObjectError(User)

        task_list_mng = list_manager.ListManager(self.storage)

        # Get all user lists
        user_task_lists = self.get_user_lists(user.login)

        # Remove list or user from list
        for task_list in user_task_lists:
            task_list.is_standard = False
            if task_list.users.__len__() == 1:
                task_list_mng.remove_list(task_list, user)
            else:
                task_list_mng.remove_user_from_list(task_list,user,user.login)

        # Remove user
        self.storage.remove_user(user.id)

        # Log info
        self.logger.info("The user with id: {} was removed".format(user.id))

    @write_log
    def change_user(self, user, email=None, login=None, first_name=None,
                    last_name=None, password=None):
        """ Change user params and update data in database

        Keyword arguments:
            user -- user to be changed
            email -- new user email
            login -- new user login
            first_name -- new user first name
            second_name -- new user second name
            password -- new user password

        Raises:
            NotExistingObjectError -- if user is None
            TypeError -- if changed params is not dict
        """

        if not isinstance(user, User):
            raise exceptions.NotExistingObjectError

        if email is not None:
            user = self.change_user_email(user, email)

        if login is not None:
            user = self.change_user_login(user, login)

        if first_name is not None:
            user = self.change_user_first_name(user, first_name)

        if last_name is not None:
            user = self.change_user_last_name(user, last_name)

        if password is not None:
            user = self.change_user_password(user, password)

        # Change user in storage
        self.storage.change_user(user)

        return user

    # region Methods to change user fields
    @write_log
    def change_user_email(self, user, new_email):
        """ Change user email and update data in database

        Keyword arguments:
            user -- user whose email is changing
            new_email -- new user email

        Raises:
            ExistingUserByEmail -- if user with new_email already exist
        """

        users = self.storage.get_users()

        # Check existance user by email
        if any(u.email == new_email for u in users):
            raise exceptions.ExistingUserByEmailError(new_email)

        # Change user
        user.change_email(new_email)

        # Log info
        self.logger.info("The email of the user with id: {} was changed to {}."
                         .format(user.id, user.email))

        return user

    @write_log
    def change_user_login(self, user, new_login):
        """ Change user login and update data in database

        Keyword arguments:
            user -- user whose login is changing
            new_login -- new user login

        Raises:
            ExistingUserByLoginError -- if user with new_login already exists
        """

        users = self.storage.get_users()

        # Check existance user by login
        if any(u.login == new_login for u in users):
            raise exceptions.ExistingUserByLoginError(new_login)

        user_task_lists = self.get_user_lists(user.login)

        # Change list of users in each user task list
        for task_list in user_task_lists:
            index = task_list.users.index(user.login)
            task_list.users[index] = new_login
            self.storage.change_task_list(task_list)

        user.change_login(new_login)

        # Log info
        self.logger.info("The login of the user with id: {} was changed to {}."
                         .format(user.id, user.login))

        return user

    @write_log
    def change_user_password(self, user, new_password):
        """ Change user password and update data in database

        Keyword arguments:
            user -- user whose password is changing
            new_password -- new user password
        """

        user.change_password(new_password)

        # Log info
        self.logger.info("The password of the user with id: {} was changed to {}."
                         .format(user.id, user.password))

        return user

    @write_log
    def change_user_first_name(self, user, new_first_name):
        """ Change user first name and update data in database

        Keyword arguments:
            user -- user whose first name is changing
            new_first_name -- new user first name
        """
        if new_first_name is None:
            new_first_name = ""

        user.change_first_name(new_first_name)

        # Log info
        self.logger.info("The first name of the user with id: {} was changed to {}."
                         .format(user.id, user.first_name))

        return user

    @write_log
    def change_user_last_name(self, user, new_last_name):
        """ Change user last name and update data in database

        Keyword arguments:
            user -- user whose last name is changing
            new_last_name -- new user last name
        """
        if new_last_name is None:
            new_last_name = ""

        user.change_last_name(new_last_name)

        # Log info
        self.logger.info("The last name of the user with id: {} was changed to {}."
                         .format(user.id, user.last_name))

        return user

    # endregion

    def get_user_lists(self, user_login):
        """ Return all user task lists

        Keyword arguments:
            user_login -- user login to get task list
        """

        user_task_lists = []
        task_lists = self.storage.get_task_lists()

        # Get all user lists
        for task_list in task_lists:
            if user_login in task_list.users:
                user_task_lists.append(task_list)

        # Sort list by priority
        user_task_lists.sort(key=lambda t: t.priority.value)

        return user_task_lists

    def get_user_tasks(self, user_login):
        """ Return all user tasks

        Keyword arguments:
            user_login -- user login to get tasks
        """

        user_lists = self.get_user_lists(user_login)
        user_tasks = []
        list_mng = list_manager.ListManager(self.storage)

        for user_list in user_lists:
            # Get all list tasks
            list_tasks = list_mng.get_list_tasks(user_list.id)

            user_tasks = user_tasks + list_tasks

        # Sort task by priority
        user_tasks.sort(key=lambda t: t.priority.value)

        return user_tasks

    def get_user_unfinished_tasks(self, user_login):
        """ Return all user tasks

        Keyword arguments:
            user_login -- user login to get tasks
        """

        user_lists = self.get_user_lists(user_login)
        user_tasks = []
        list_mng = list_manager.ListManager(self.storage)

        for user_list in user_lists:
            # Get all list tasks
            list_tasks = list_mng.get_list_unfinished_tasks(user_list.id)

            user_tasks = user_tasks + list_tasks

        # Sort task by priority
        user_tasks.sort(key=lambda t: t.priority.value)

        user_tasks_with_deadline = []
        user_tasks_without_deadline = []
        for user_task in user_tasks:
            if user_task.deadline is not None:
                user_tasks_with_deadline.append(user_task)
            else:
                user_tasks_without_deadline.append(user_task)
        user_tasks_with_deadline.sort(key=lambda t: t.deadline)
        user_tasks = user_tasks_with_deadline + user_tasks_without_deadline
        return user_tasks

    def get_user_finished_tasks(self, user_login):
        """ Return all user tasks

        Keyword arguments:
            user_login -- user login to get tasks
        """

        user_lists = self.get_user_lists(user_login)
        user_tasks = []
        list_mng = list_manager.ListManager(self.storage)

        for user_list in user_lists:
            # Get all list tasks
            list_tasks = list_mng.get_list_finished_tasks(user_list.id)

            user_tasks = user_tasks + list_tasks

        # Sort task by priority
        user_tasks.sort(key=lambda t: t.priority.value)

        return user_tasks

    def get_user_failed_tasks(self, user_login):
        """ Return all user tasks

        Keyword arguments:
            user_login -- user login to get tasks
        """

        user_lists = self.get_user_lists(user_login)
        user_tasks = []
        list_mng = list_manager.ListManager(self.storage)

        for user_list in user_lists:
            # Get all list tasks
            list_tasks = list_mng.get_list_failed_tasks(user_list.id)

            user_tasks = user_tasks + list_tasks

        # Sort task by priority
        user_tasks.sort(key=lambda t: t.priority.value)

        return user_tasks

    def show_full_user_info(self, user):
        """ Show full information about user

        Keyword_arguments:
            user -- user object
        """

        print('\n--------------------------------------')

        print("ID: {}\nLogin: {}\nEmail: {} ".format(user.id, user.login,
                                                     user.email))
        full_name = user.first_name + " " + user.last_name
        if full_name != " ":
            print("\nUser name: {}".format(full_name))
        print('--------------------------------------\n')

    def show_short_user_info(self, user):
        """ Show short information about user

        Keyword_arguments:
            user -- user object
        """

        print("\tlogin: {} \n\temail: {} \n\tID: {}\n ".format(user.login, user.email,
                                                                          user.id))