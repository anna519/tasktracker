""" This module includes the User entity
User entity is a user who can has tasklists and complete tasks. User necessery has
unique email, unique login and password. He also has first name and last name.

Example:
    >>> import lib_task_tracker.entities.task_list.task_list
    >>> import lib_task_tracker.entities.user.user


    >>> user = lib_task_tracker.entities.user.user.User('email', 'login', 'password')
    >>> task_list = lib_task_tracker.entities.task_list.task_list.TaskList('TaskList'. user.login)

The functions that can be performed with the user:
change email, change password, change login, change first name, change last name.
"""


from uuid import uuid1


class User:

    """ Class that represents User entity

    Fields:
        email -- user email
        login -- user login
        password -- user password
        first_name -- user first name
        last_name -- user last name
        id -- user id
    """

    def __init__(self, email, login, password, first_name, last_name, id = None):

        self.email = email
        self.login = login
        self.password = password
        self.first_name = first_name
        self.last_name = last_name
        if id is None:
            self.id = uuid1()
        else:
            self.id = id

    # region Methods for User fields
    def change_email(self, new_email):
        """ Change user email

        Keyword arguments:
            new_email -- new user email
        """

        self.email = new_email

    def change_login(self, new_login):
        """ Change user login

        Keyword arguments:
            new_login -- new user login
        """

        self.login = new_login

    def change_password(self, new_password):
        """ Change user password

        Keyword arguments:
            new_password -- new user password
        """

        self.password = new_password

    def change_first_name(self, new_first_name):
        """ Change user first name

        Keyword arguments:
            new_first_name -- new user first name
        """

        self.first_name = new_first_name

    def change_last_name(self, new_last_name):
        """ Change user last name

        Keyword arguments:
            new_last_name -- new user last name
        """

        self.last_name = new_last_name

    # endregion
