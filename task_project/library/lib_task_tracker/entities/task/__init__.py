""" This package includes enums and classes that represent the main logic for Task.

    --- Task ---
    Task entity is a task that user wants to complete. The task has a name. Also it has
    priority (by default -- AVERAGE), deadline, dependent task, status. Task can be
    subtask for other task.
    If task is subtask for other task it means if main task is completed than subtasks is done too.
    if task is dependent task for main task it means dependent task can't be change your status
    value before main task is completed.

    --- TaskManager ---
    TaskManager is a class that helps manage Task entity with database.
    It helps create, update, remove tasks.

    --- Priority ---
    Task priority is a enum. It can be HIGH, AVERAGE, LOW.

    --- Status ---
    Task status is a enum. It can be NOT_STARTED, STARTED, DONE, FAILED.


Modules:
    list_manager -- includes the class that represents ListManager
    priority_enum -- includes enum that represents TaskList priority
    task_list -- includes the class that represents the TaskList
"""