""" This module includes task Status enum"""


from enum import Enum


class Status(Enum):

    """ Enum for task status

    Statuses: NOT_STARTED, STARTED, DONE, FAILED
    """

    NOT_STARTED = 0
    STARTED = 1
    DONE = 2
    FAILED = 3
