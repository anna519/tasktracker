""" This module includes the Task entity
Task entity is a task that user wants to complete. The task has a name. Also it has
priority (by default -- AVERAGE), deadline, dependent task, status. Task can be
subtask for other task.
If task is subtask for other task it means if main task is completed than subtasks is done too.
if task is dependent task for main task it means dependent task can't be change your status
value before main task is completed.

Example:
    >>> import lib_task_tracker.entities.task_list.task_list
    >>> import lib_task_tracker.entities.user.user
    >>> import lib_task_tracker.entities.task.task


    >>> user = lib_task_tracker.entities.user.user.User('email', 'login', 'password')
    >>> task_list = lib_task_tracker.entities.task_list.task_list.TaskList('TaskList'. user.login)
    >>> task = lib_task_tracker.entities.task.task.Task('new_task', task_list.id)

The functions that can be performed with the task:
change name, change priority, change status, change deadline, add parent, remove parent,
add dependent/blocking link, remove dependent/blocking link.

"""


from uuid import uuid1
from lib_task_tracker.entities.task import priority_enum, status_enum
from datetime import datetime


class Task:
    """ Class that represents Task entity

    Fields:
        name -- task name
        task_list_id -- id of the tasklist that contains task
        deadline -- task deadline
        priority -- task priority
        id -- task id
        creation_datetime -- time of the task creation
        parent -- task parent id if task is subtask
        status -- task status
        is_dependent -- true if task is dependent
        blocks -- tasks ids that are blocked by this task
    """

    def __init__(self, name, task_list_id, deadline=None,
                 priority=priority_enum.Priority.AVERAGE, id=None):
        self.name = name
        self.task_list_id = task_list_id
        if id is None:
            self.id = uuid1()
        else:
            self.id = id
        self.deadline = deadline
        self.creation_datetime = datetime.now()
        self.finished_datetime = None
        self.parent = None
        self.priority = priority
        self.status = status_enum.Status.NOT_STARTED
        self.is_dependent = False
        self.blocks = []
        self.periodicity = None
        self.period = None

    def __eq__(self, other):
        return self.id == other.id

    def __str__(self):
        return "Name: " + self.name

    # Methods for tasks fields

    def change_name(self, new_name):
        """ Changes the task name

        Keyword arguments:
             new_name -- new task name
        """
        self.name = new_name

    def change_deadline(self, new_deadline):
        """ Changes the task deadline

        Keyword arguments:
             new_deadline -- new task deadline
        """
        if new_deadline is None:
            self.deadline = None
            return
        else:
            self.deadline = new_deadline

        if self.deadline < datetime.now():
            if self.status is not status_enum.Status.DONE:
                self.status = status_enum.Status.FAILED
        elif self.deadline > datetime.now():
            if self.status is status_enum.Status.FAILED:
                self.status = status_enum.Status.NOT_STARTED

    def change_priority(self, new_priority):
        """ Changes the task priority

        Keyword arguments:
             new_priority -- new task priority
        """

        self.priority = new_priority

    def change_status(self, new_status):
        """ Changes the task status

        Keyword arguments:
             new_status -- new task status
        """

        self.status = new_status

    def change_period(self, new_period):
        self.period = new_period

    # Methods for subtasks
    def add_parent(self, parent_task_id):
        """ Changes the task parent

        Keyword arguments:
             parent_task_id -- parent task id
        """

        self.parent = parent_task_id

    def remove_parent(self):
        """ Changes the task parent"""

        self.parent = None

    # Methods for working with logic links

    def add_dependent_link(self):
        self.is_dependent = True

    def remove_dependent_link(self):
        self.is_dependent = False

    def add_blocking_link(self, dependent_task_id):
        self.blocks.append(dependent_task_id)

    def remove_blocking_link(self, dependent_task_id):
        index = self.blocks.index(dependent_task_id)
        del self.blocks[index]

