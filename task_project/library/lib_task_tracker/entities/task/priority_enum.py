""" This module includes task Priority enum"""


from enum import Enum


class Priority(Enum):

    """ Enum for task priority

    Priorities: HIGH, AVERAGE, LOW
    """

    HIGH = 0
    AVERAGE = 1
    LOW = 2
