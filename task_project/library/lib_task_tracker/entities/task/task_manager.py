""" This module includes functions for the Task entity
TaskManager is a class that helps manage Task entity with database.

Example:
    >>> import os
    >>> import lib_task_tracker.entities.user.user_manager
    >>> import lib_task_tracker.entities.task.task_manager


    >>> path = os.path.expanduser('~/.TaskTracker/Data')
    >>> user_manager = lib_task_tracker.entities.user.user_manager.UserManager(storage_path=path)
    >>> user = user_manager.create_user('email', 'login', 'password')

    >>> list_manager = lib_task_tracker.entities.task.task_manager.TaskManager(storage_path=path)
    >>> task_list = list_manager.create_list('new_task_list', user)

The functions that can be performed with the task:
create_task, change_task, remove_task, add_dependent_task, remove dependent_task,
add_subtask, remove_subtask, change_period, get_task_subtasks, get_blocking_task,
show_full_task_info, show_short_task_info

"""


from datetime import datetime
import time, threading
from dateutil.relativedelta import relativedelta
from lib_task_tracker.entities.task.task import Task
from lib_task_tracker.storage.database_manager import DataBaseManager
from lib_task_tracker.storage.json.json_manager import JsonManager
from lib_task_tracker.entities.user.user import User
from lib_task_tracker.entities.task_list.task_list import TaskList
from lib_task_tracker.entities.task.status_enum import Status
from lib_task_tracker.entities.task.priority_enum import Priority
from lib_task_tracker.entities.task_list import list_manager
from lib_task_tracker import exceptions
from lib_task_tracker.logger import get_logger, write_log
from lib_task_tracker.entities.permission import check_permissions


STORAGE_PATH = ""

class TaskManager:
    """ Performs actions for work with Task entity

    Fields:
    storage -- storage object that used for work with data
    logger -- the app logger
    storage_path -- path to store data
    """

    def __init__(self, storage=None, storage_path=None):
        """ Initialize the object

        Keyword arguments:
            storage -- the storage to save data
            storage_path -- the path where the data store
        """

        self.logger = get_logger()

        if storage_path is None and storage is None:
            raise exceptions.NotValidParameterError(storage, 'storage')

        if storage and not isinstance(storage, DataBaseManager):
            raise TypeError("The storage isn't of the type DataBaseManager")

        if storage:
            self.storage = storage
            return

        self.storage = DataBaseManager(JsonManager(storage_path))

    @write_log
    def create_task(self, name, current_user, task_list=None, deadline=None,
                    priority=None, id=None):
        """ Create a task object and add data to the database

        Keyword arguments:
            name -- task name
            current_user -- user who create the task
            task_list -- task list of the task (None)
            deadline -- task deadline (None)
            priority -- task priority (None)

        Raises:
            NotExistingObjectError -- if current_user doesn't exist
            TypeError -- if current user isn't User object
        """

        if current_user is None:
            raise exceptions.NotExistingObjectError(User)

        if not isinstance(current_user, User):
            raise TypeError("The current user is not User object.")

        if deadline is not None:
            deadline = self.get_valid_date(deadline)

        if priority is None:
            priority = Priority.AVERAGE
        else:
            priority = self.get_valid_task_priority(priority)

        # Get standart task list for task
        if task_list is None:
            from lib_task_tracker.entities.user import user_manager

            user_mng = user_manager.UserManager(self.storage)
            user_lists = user_mng.get_user_lists(current_user.login)
            for user_list in user_lists:
                if user_list.name == "Inbox":
                    task_list = user_list
        else:
            if not isinstance(task_list, TaskList):
                raise TypeError("The task list is not TaskList object.")

            # Check permission
            if current_user.login not in task_list.users:
                raise PermissionError("The user with login: {} hasn't permission "
                                      "to task list with id: {}".format(current_user.login,
                                                                        task_list.id))

        if not isinstance(task_list, TaskList):
            raise TypeError("The task list is not TaskList object.")

        new_task = Task(name, task_list.id, deadline, priority, id)

        # Add task to storage
        self.storage.add_task(new_task)

        # Log info
        self.logger.info("The task was added with id: {}".format(new_task.id))

        return new_task

    @check_permissions
    @write_log
    def remove_task(self, task, current_user):
        """ Delete the task object

        Keyword arguments:
            task -- task to remove
            current_user -- user who remove the task

        Raises:
            TypeError -- if task isn't Task object
        """

        if not isinstance(task, Task):
            raise TypeError("The deleting task is not the Task object.")

        # For all dependent task remove links
        if task.blocks.__len__() != 0:
            for dependent_task_id in task.blocks:
                dependent_task = self.storage.get_task_by_id(dependent_task_id)
                self.remove_dependent_task(task, current_user, dependent_task)

        # If task is dependent, remove link

        if task.is_dependent is True:
            blocking_task = self.get_blocking_task(task)
            if blocking_task is not None:
                self.remove_dependent_task(blocking_task, current_user, blocking_task)

        sub_tasks = self.get_task_subtasks(task.id)

        # If task has sub_tasks, remove it recursively
        if len(sub_tasks) != 0:
            for sub_task in sub_tasks:
                self.remove_task(sub_task, current_user)

        # Log info
        self.logger.info("The task with id {} was removed.".format(task.id))

        # Remove task from storage
        self.storage.remove_task(task.id)

    @check_permissions
    @write_log
    def change_task(self, task, current_user, changed_params):
        """ Change task params and update data in database

        Keyword arguments:
            task -- task to be changed
            current_user -- user who change the task
            changed_params -- task attributes to be changed (dictionary)

        Raises:
            NotExistingObjectError -- if task is None
            TypeError -- if task isn't Task object or changed_params isn't dict
        """

        if task is None:
            raise exceptions.NotExistingObjectError(Task)

        if not isinstance(task, Task):
            raise TypeError("The changing task is not the Task object.")
        task_params = {
            "deadline": self.change_task_deadline,
            "name": self.change_task_name,
            "priority": self.change_task_priority,
            "status": self.change_task_status
        }

        for param in task_params:
            if param in changed_params:
                task = task_params[param](task, changed_params[param])

        # Change task in storage
        self.storage.change_task(task)

        return task

    @write_log
    def change_task_name(self, task, new_name):
        """ Change the task name

        Keyword arguments:
            task -- task to change
            new_name -- new task name
        """

        # Change task name
        task.change_name(new_name)

        # Log info
        self.logger.info("The name of the task with id: {} was changed to {}"
                         .format(task.id, task.name))

        return task

    @write_log
    def change_task_deadline(self, task, new_deadline):
        """ Change task deadline

        Keyword arguments:
            task -- task to change
            new_deadline -- new task deadline

        Raises:
            NotValidParameterError -- if new_deadline is incorrect
        """

        # Get valid deadline
        new_deadline = self.get_valid_date(new_deadline)

        # Change deadline
        task.change_deadline(new_deadline)

        # Log info
        self.logger.info("The deadline of the task with id: {} was changed to {}"
                         .format(task.id, task.deadline))

        return task

    @write_log
    def change_task_status(self, task, new_status):
        """ Change task status

        Keyword arguments:
            task -- task to change
            new_status -- new task status

        Raises:
            NotValidParameterError -- if new_status is incorrect
        """

        # Get valid task status
        new_status = self.get_valid_task_status(new_status)

        # If task is dependent, raise error
        if task.is_dependent:
            raise exceptions.TaskIsBlockedError()
        else:
            # If task is done, remove links between dependent task and
            # change sub_task status to done
            if new_status == Status.DONE:
                # Remove links with dependent tasks
                for dependent_task_id in task.blocks:
                    dependent_task = self.storage.get_task_by_id(dependent_task_id)
                    dependent_task.is_dependent = False
                    self.storage.change_task(dependent_task)
                task.blocks = []

                sub_tasks = self.get_task_subtasks(task.id)
                # Change sub_task status to done
                for sub_task in sub_tasks:
                    self.change_task_status(sub_task, new_status)
                    self.storage.change_task(sub_task)

            # Change task status
            task.change_status(new_status)

            # Log info
            self.logger.info("The status of the task with id: {} was changed to {}"
                             .format(task.id, task.status.name))

        return task

    @write_log
    def change_task_priority(self, task, new_priority):
        """ Change task priority

        Keyword arguments:
            task -- task to change
            new_priority -- new task priority

        Raises:
            NotValidParameterError -- if new_priority is incorrect
        """

        # Get valid priority
        new_priority = self.get_valid_task_priority(new_priority)

        # Change task priority
        task.change_priority(new_priority)

        # Log info
        self.logger.info("The priority of the task with id: {} was changed to {}"
                         .format(task.id, task.priority.name))

        return task

    @check_permissions
    @write_log
    def add_sub_task(self, parent_task, current_user, sub_task_name,
                     subtask_deadline=None, subtask_priority=None):
        """ Create the subtask and data to the database

        Keyword arguments:
            parent_task -- parent task for this subtask
            sub_task_name -- subtask name
            current_user -- user who create subtask
            subtask_deadline -- sutask deadline (None)
            subtask_priority -- subtask priority (None)

        Raises:
            NotExistingObjectError -- if parent_task is None
            NotValidParameterError -- if deadline or priority is incorrect
            TypeError -- if parent_task is not Task object
        """

        if parent_task is None:
            raise exceptions.NotExistingObjectError(Task)

        if not isinstance(parent_task, Task):
            raise TypeError("Parent task is not Task object.")

        if subtask_priority is None:
            subtask_priority = Priority.AVERAGE
        else:
            subtask_priority = self.get_valid_task_priority(subtask_priority)

        # Get task list of parent task
        task_list = self.storage.get_task_lists_by_id(parent_task.task_list_id)

        # Create sub_task
        sub_task = self.create_task(sub_task_name, current_user, task_list,
                                    subtask_deadline, subtask_priority)
        sub_task.add_parent(parent_task.id)

        self.storage.change_task(sub_task)

        # Log info
        self.logger.info("The sub task with id: {} was added."
                         .format(sub_task.id))

        return True

    @check_permissions
    @write_log
    def remove_subtask(self, main_task, current_user, sub_task):
        """ Remove subtask from main task

        Keyword arguments:
            main_task -- from it remove subtask
            sub_task -- remove it from main task

        Raises:
            NotExistingObjectError -- if main task or subtask doesn't exist
            TypeError -- if main task or subtask is not Task object
        """

        if main_task is None:
            raise exceptions.NotExistingObjectError(Task)

        if not isinstance(main_task, Task):
            raise TypeError("The main task isn't Task object.")

        if sub_task is None:
            raise exceptions.NotExistingObjectError(Task)

        if not isinstance(sub_task, Task):
            raise TypeError("The sub task isn't Task object.")

        # Check sub_task parent
        if str(sub_task.parent) == str(main_task.id):
            sub_task.remove_parent()
            self.storage.change_task(sub_task)
            self.logger.info("The sub task with id: {} was deleted."
                             .format(sub_task.id))
            return True
        else:
            self.logger.info("The task with id: {} isn't subtask for task with id: {}"
                             .format(sub_task.id, main_task.id))
            return False

    @check_permissions
    @write_log
    def add_dependent_task(self, main_task, current_user, dependent_task_name,
                           dependent_task_deadline=None, dependent_task_priority=None):
        """ Create and add dependent task

        Keyword arguments:
            main_task -- blocking task
            current_user -- current user
            dependent_task_name -- dependent task name
            dependent_task_deadline -- dependent task deadline (None)
            dependent_task_priority -- dependent task priority (None)

        Raises:
            NotExistingObjectError -- if main task doesn't exist
            TypeError -- if main task is not Task object
        """

        if main_task is None:
            raise exceptions.NotExistingObjectError(Task)

        if not isinstance(main_task, Task):
            raise TypeError("The main task isn't Task object.")

        if main_task.status != Status.DONE:
            task_list = self.storage.get_task_lists_by_id(main_task.task_list_id)

            dependent_task = self.create_task(dependent_task_name,current_user,
                                              task_list, dependent_task_deadline,
                                              dependent_task_priority)

            dependent_task.is_dependent = True
            main_task.blocks.append(dependent_task.id)

            self.storage.change_task(main_task)
            self.storage.change_task(dependent_task)
            self.logger.info("The dependent task with id: {} was added."
                             .format(dependent_task.id))
            return True

        else:
            self.logger.info("The main task with id: {} is already done."
                             .format(main_task.id))
            return False

    @check_permissions
    @write_log
    def remove_dependent_task(self, main_task, current_user, dependent_task):
        """ Remove dependent task from main task

        Keyword arguments:
            main_task -- from it remove subtask
            dependent_task -- remove it from main task

        Raises:
            NotExistingObjectError -- if main task or dependent task doesn't exist
            TypeError -- if main task or dependent task is not Task object
        """

        if main_task is None:
            raise exceptions.NotExistingObjectError(Task)

        if not isinstance(main_task, Task):
            raise TypeError("The main task isn't Task object.")

        if dependent_task is None:
            raise exceptions.NotExistingObjectError(Task)

        if not isinstance(dependent_task, Task):
            raise TypeError("The dependent task isn't Task object.")

        if dependent_task.id in main_task.blocks:
            index = main_task.blocks.index(dependent_task.id)
            del main_task.blocks[index]

            dependent_task.is_dependent = False
            self.storage.change_task(dependent_task)
            self.storage.change_task(main_task)
            self.logger.info("The dependent task with id: {} was removed."
                             .format(dependent_task.id))
            return True
        else:
            self.logger.info("The task with id: {} isn't dependent task for task with id: {}."
                             .format(dependent_task.id, main_task.id))
            return False

    def get_task_list_of_task(self, task):

        task_list = self.storage.get_task_lists_by_id(task.task_list_id)
        return task_list

    @write_log
    @check_permissions
    def change_period(self, task, current_user_id, year, month, day, hour, number):
        task_list = self.storage.get_task_lists_by_id(task.task_list_id)
        next_call = datetime.now() + relativedelta(years=+year, months=+month, days=+day,
                                                   hours=+hour)
        next_call.strftime("%H-%M %d-%m-%Y")
        for _ in range(number):
            self.create_task(task.name, current_user_id, task_list, str(next_call.strftime("%H-%M %d-%m-%Y")),
                             task.priority)
            next_call = next_call + relativedelta(years=+year, months=+month, days=+day,
                                                    hours=+hour)

    def get_task_subtasks(self, task_id):
        """ Return all task subtasks

        Keyword arguments:
            task_id -- task id to get subtasks
        """

        sub_tasks = []
        tasks = self.storage.get_tasks()

        # Get sub_tasks for task
        for task in tasks:
            if task.parent == task_id:
                sub_tasks.append(task)

        return sub_tasks

    def get_blocking_task(self, task):
        """ Return blocking task

        Keyword arguments:
            task -- task to get blocking task
        """

        tasks = self.storage.get_tasks()

        # Find blocking task for task
        for each_task in tasks:
            if task.id in each_task.blocks:
                blocking_task = each_task
                return blocking_task

    def get_valid_date(self, date):
        """ Check date to get valid date

        Keywords arguments:
            date -- date to check

        Raises:
            NotValidParameterError -- if date is incorrect
        """

        if date is not None:
            try:
                date = datetime.strptime(date, "%H-%M %d-%m-%Y")
                return date
            except (ValueError, TypeError):
                try:
                    date = datetime.strptime(date, "%d-%m-%Y")
                    return date
                except (ValueError, TypeError):
                    raise exceptions.NotValidParameterError(date, "date")

    def get_valid_task_priority(self, priority):
        """ Check task priority to get valid priority

        Keywords arguments:
            priority -- task priority to check

        Raises:
            NotValidParameterError -- if priority is incorrect
        """

        if isinstance(priority, Priority):
            return priority

        elif isinstance(priority, int):
            if 0 <= priority <= 2:
                return Priority(priority)

        elif isinstance(priority, str):

            try:
                priority_number = int(priority)
                if 0 <= priority_number <= 2:
                    return Priority(priority_number)

            except ValueError:
                priority = priority.upper()

                try:
                    return Priority[priority]

                except KeyError:
                    pass

        raise exceptions.NotValidParameterError(priority, "priority")

    def get_valid_task_status(self, status):
        """ Check task status to get valid status

        Keywords arguments:
            status -- task status to check

        Raises:
            NotValidParameterError -- if status is incorrect
        """

        if isinstance(status, Status):
            return status

        elif isinstance(status, int):
            if 0 <= status <= 3:
                return Status(status)

        elif isinstance(status, str):

            try:
                status_number = int(status)
                if 0 <= status_number <= 3:
                    return Status(status_number)

            except ValueError:
                status = status.upper()

                try:
                    return Status[status]

                except KeyError:
                    pass

        raise exceptions.NotValidParameterError(status, "status")

    def show_full_task_info(self, task):
        """ Show full information about task

        Keyword_arguments:
            task -- task object
        """
        print('\n--------------------------------------')

        print("---{}---".format(task.status.name))

        task_list_mng = list_manager.ListManager(self.storage)
        task_list = self.storage.get_task_lists_by_id(task.task_list_id)

        print("Task list:")
        task_list_mng.show_short_list_info(task_list)
        print("\nID: {}\nName: {}\nThe date of creation: {}\n"
              "Priority: {}\n"
              .format(task.id, task.name, task.creation_datetime,
               task.priority.name))
        if task.parent is not None:
            print("Parent: {}".format(task.parent))

        if task.deadline is not None:
            print("Deadline: {}".format(task.deadline))

        sub_tasks = self.get_task_subtasks(task.id)
        if sub_tasks:
            print("\nSub tasks:")
            for sub_task in sub_tasks:
                self.show_short_task_info(sub_task)

        if task.blocks:
            print("\nDependent tasks:")
            for dependent_task_id in task.blocks:
                dependent_task = self.storage.get_task_by_id(dependent_task_id)
                self.show_short_task_info(dependent_task)
        blocking_task = self.get_blocking_task(task)
        if blocking_task:
            print("Blocking task: ")
            self.show_short_task_info(blocking_task)

        print('--------------------------------------\n')

    def show_short_task_info(self, task):
        """ Show short information about task

        Keyword_arguments:
            task -- task object
        """
        print("\tname: {}\n\tstatus: {}\n\t priority: {}\n"
              .format(task.name, task.status, task.priority.name))
