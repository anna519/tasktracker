""" This package includes main project entities and functions with it.

    --- Task ---
    Task entity is a task that user wants to complete. The task has a name. Also it has
    priority (by default -- AVERAGE), deadline, dependent task, status. Task can be
    subtask for other task.
    If task is subtask for other task it means if main task is completed than subtasks is done too.
    if task is dependent task for main task it means dependent task can't be change your status
    value before main task is completed.

    --- TaskList ---
    TaskList entity is a list that unites tasks. TaskList has such info as name,
    is_standart, priority and users who have access to tasklist.

    --- User ---
    User entity is a user who can has tasklists and complete tasks. User necessery has
    unique email, unique login and password. He also has first name and last name.

    Classes UserManager, ListManager, TaskManager exists to manage User, TaskList and Task in the database.

    --- UserManager ---
    UserManager is a class that helps manage User entity with database.
    It helps create, update, remove user.

    --- ListManager ---
    ListManager is a class that helps manage List entity with database.
    It helps create, update, remove task list.

    --- TaskManager ---
    Task Manager is a class that helps manage Task entity with database.
    It helps create, update, remove task.

    Packages:
    task -- includes enums and classes that represent the main logic for Task
    task_list -- includes enums and classes that represent the main logic for TaskList
    user -- includes classes that represent the main logic for User

    Module:
    permission -- includes the decorator that checks the user permissions
"""