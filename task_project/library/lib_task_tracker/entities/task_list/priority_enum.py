""" This module includes tasklist Priority enum"""


from enum import Enum


class Priority(Enum):

    """ Enum for task list priority

    Priorities: HIGH, AVERAGE, LOW
    """

    HIGH = 0
    AVERAGE = 1
    LOW = 2
