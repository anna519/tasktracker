""" This package includes enums and classes that represent the main logic for TaskList.

    --- TaskList ---
    TaskList entity is a list that unites tasks. TaskList has such info as name,
    is_standart, priority and users who have access to tasklist.

    --- ListManager ---
    ListManager is a class that helps manage TaskList entity with database.
    It helps create, update, remove tasklist.

    --- Priority ---
    TaskList priority is a enum. It can be HIGH, AVERAGE, LOW.


Modules:
    list_manager -- includes the class that represents ListManager
    priority_enum -- includes enum that represents TaskList priority
    task_list -- includes the class that represents the TaskList
"""