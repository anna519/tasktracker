""" This module includes the TaskList entity
TaskList entity is a list that unites tasks.

Example:
    >>> import lib_task_tracker.entities.task_list.task_list
    >>> import lib_task_tracker.entities.user.user


    >>> user = lib_task_tracker.entities.user.user.User('email', 'login', 'password')
    >>> task_list = lib_task_tracker.entities.task_list.task_list.TaskList('TaskList'. user.login)

The functions that can be performed with the task list:
change name, change priority, add user to task, remove user from task
"""

import uuid
from lib_task_tracker.entities.task_list.priority_enum import Priority


class TaskList:
    """ Class that represent TaskList entity

    Fields:
    name -- task list name
    users -- logins of users who have current task list
    priority -- task list priority
    id -- task list id
    is_standart -- true if task list is standart
    """
    def __init__(self, name, user_login, priority=Priority.AVERAGE,
                 is_standard=False, id=None):
        self.name = name
        self.users = []
        self.users.append(user_login)

        self.priority = priority

        self.is_standard = is_standard

        if id is None:
            self.id = uuid.uuid1()
        else:
            self.id = id

    # Methods for list of tasks
    def change_name(self, new_name):
        """ Change task list name

        Keyword arguments:
            new_name -- new task list name
        """

        self.name = new_name

    def change_priority(self, new_priority):
        """ Change task list priority

        Keyword arguments:
            new_priority -- new task list priority
        """

        self.priority = Priority(new_priority)

    # Methods for user
    def add_user(self, user_login):
        """ Add user to task list

        Keyword arguments:
            user login -- user login to add to task list
        """

        self.users.append(user_login)

    def remove_user(self, user_login):
        """ Remove user from task list

        Keyword arguments:
            user login -- user login to remove from task list
        """

        self.users.remove(user_login)

