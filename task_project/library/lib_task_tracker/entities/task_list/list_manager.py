""" This module includes functions for the TaskList entity
ListManager is a class that helps manage TaskList entity. Task list has name, priority
and list of users logins who have access to task list.

Example:
    >>> import os
    >>> import lib_task_tracker.entities.user.user_manager
    >>> import lib_task_tracker.entities.task_list.list_manager


    >>> path = os.path.expanduser('~/.TaskTracker/Data')
    >>> user_manager = lib_task_tracker.entities.user.user_manager.UserManager(storage_path=path)
    >>> user = user_manager.create_user('email', 'login', 'password')

    >>> list_manager = lib_task_tracker.entities.task_list.list_manager.ListManager(storage_path=path)
    >>> task_list = list_manager.create_list('new_task_list', user)

The functions that can be performed with the task list:
create_list, change_task_list, remove_list, add_user_to_list, remove_user_from_list,
get_user_lists, get_list_tasks, get_list_unfinished_tasks, get_list_finished_tasks,
show_full_list_info, show_short_list_info

"""


from lib_task_tracker.entities.task_list.task_list import TaskList
from lib_task_tracker.entities.task_list.priority_enum import Priority
from lib_task_tracker.entities.user.user import User
from lib_task_tracker.storage.database_manager import DataBaseManager
from lib_task_tracker.storage.json.json_manager import JsonManager
from lib_task_tracker import exceptions
from lib_task_tracker.logger import get_logger, write_log
from lib_task_tracker.entities.permission import check_permissions


class ListManager:
    """ Performs actions for work with Task List

    Fields:
    storage -- storage object that used for work with data
    logger -- the app logger
    storage_path -- path to store data
    """

    def __init__(self, storage=None, storage_path=None):
        """ Initialize the object

        Keyword arguments:
            storage -- the storage to save data
            storage_path -- the path where the data store
        """

        self.logger = get_logger()

        if storage_path is None and storage is None:
            raise exceptions.NotValidParameterError(storage, 'storage')

        if storage and not isinstance(storage, DataBaseManager):
            raise TypeError("The storage isn't of the type DataBaseManager")

        if storage:
            self.storage = storage
            return

        self.storage = DataBaseManager(JsonManager(storage_path))

    @write_log
    def create_list(self, name, current_user, priority=None, is_standard=False,
                    id=None):
        """ Create a task list object and add data to the database

        Keyword arguments:
            name -- task list name
            current_user -- user who create the task list

        Raises:
            NotExistingObjectError -- if current user is None
            TypeError -- if current user is not User object
            ExistingTaskListByName -- if user already hac task list with such name
        """

        if current_user is None:
            raise exceptions.NotExistingObjectError(User)

        if not isinstance(current_user, User):
            raise TypeError("The current user is not User object.")

        if priority is None:
            priority = Priority.AVERAGE
        else:
            priority = self.get_valid_task_list_priority(priority)

        from lib_task_tracker.entities.user import user_manager

        user_mng = user_manager.UserManager(self.storage)

        user_task_lists = user_mng.get_user_lists(current_user.login)

        # Check existence task list with such name
        for task_list in user_task_lists:
            if task_list.name == name:
                raise exceptions.ExistingTaskListByNameError(name)

        # Create Task list
        new_list = TaskList(name, current_user.login, priority, is_standard, id)

        self.storage.add_task_list(new_list)

        # Log info
        self.logger.info("The  task list with id: {} was added."
                         .format(new_list.id))

        return new_list

    @check_permissions
    @write_log
    def remove_list(self, task_list, current_user):
        """ Remove task list from the database

        Keyword arguments:
            task_list -- task list to remove from the database
            current_user -- user who remove the task list

        Raises:
            NotExistingObjectError -- if task list is None
            TypeError -- if task list isn't Task list object
        """

        if task_list is None:
            raise exceptions.NotExistingObjectError(TaskList)

        if not isinstance(task_list, TaskList):
            raise TypeError("The task list is not TaskList object.")

        if task_list.is_standard:
            raise exceptions.StandartTaskListError(task_list.name)

        list_tasks = self.get_list_tasks(task_list.id)

        import lib_task_tracker.entities.task.task_manager as task_manager
        task_mng = task_manager.TaskManager(self.storage)

        # Remove all tasks in task list
        for task in list_tasks:
            task_mng.remove_task(task, current_user)


        self.storage.remove_task_list(task_list.id)

        # Log info
        self.logger.info("The  task list with id: {} was removed."
                         .format(task_list.id))

    @check_permissions
    @write_log
    def change_task_list(self, task_list, current_user, name=None, priority=None):
        """ Change task list params and update data in database

        Keyword arguments:
            task_list -- task list to be changed
            current_user -- user who change the task list
            changed_params -- task attributes to be changed (dictionary)

        Raises:
            NotExistingObjectError -- if task list is None
            TypeError -- if task list isn't TaskList object or changed_params
                         isn't dict
        """

        if task_list is None:
            raise exceptions.NotExistingObjectError(TaskList)

        if not isinstance(task_list, TaskList):
            raise TypeError("The changing task list isn't the TaskList object.")

        if task_list.is_standard:
            raise exceptions.StandartTaskListError(task_list.name)

        if name is not None:
            task_list = self.change_list_name(task_list, current_user, name)

        if priority is not None:
            task_list = self.change_list_priority(task_list, current_user, priority)

        # Change task_list in storage
        self.storage.change_task_list(task_list)

        return task_list

    @write_log
    def change_list_name(self, task_list, current_user, new_name):
        """ Change the task list name

        Keyword argum_listents:
            task -- task list to change
            current_user -- user who change the task list
            new_name -- new task list name

        Raises:
            ExistingTaskListByName -- if user already has task list with such name
        """

        from lib_task_tracker.entities.user import user_manager

        user_mng = user_manager.UserManager(self.storage)

        # Get all user task lists
        user_task_lists = user_mng.get_user_lists(current_user.login)

        # Check existence task list with such name
        for user_task_list in user_task_lists:
            if user_task_list.name == new_name:
                raise exceptions.ExistingTaskListByNameError(new_name)

        # Change task_list
        task_list.change_name(new_name)

        # Log info
        self.logger.info("The name of the task list with id: {} was changed to {}."
                         .format(task_list.id, task_list.name))

        return task_list

    @write_log
    def change_list_priority(self, task_list, current_user, new_priority):
        """ Change task list priority

        Keyword arguments:
            task_list -- task list to change
            current_user -- user who change the task list
            new_priority -- new task priority

        Raises:
            NotValidParameterError -- if new priority is incorrect
        """

        new_priority = self.get_valid_task_list_priority(new_priority)

        # Change task_list
        task_list.change_priority(new_priority)

        # Log info
        self.logger.info("The priority of the task list with id: {} was changed to {}."
                         .format(task_list.id, task_list.priority.name))

        return task_list

    @check_permissions
    @write_log
    def add_user_to_list(self, task_list, current_user, user_login):
        """ Add user to task list

        Keyword argument:
            task_list -- task list to change
            current_user -- user who change the task list
            user_login -- user login to add

        Raises:
            ExistingObjectInListError -- if user already exist in list of users
            TypeError -- if task list is not TaskList object
            StandartTaskListError -- if the task list is standart
        """

        if not isinstance(task_list, TaskList):
            raise TypeError("Task list isn't TaskList object.")

        # Check task list is standart
        if task_list.is_standard:
            raise exceptions.StandartTaskListError(task_list.name)

        # Check existence user_login in list of users
        if user_login in task_list.users:
            raise exceptions.ExistingObjectInListError(user_login, task_list.users)
        user = self.storage.get_user_by_login(user_login)

        if user is not None:
            task_list.add_user(user_login)

            # Change task list in storage
            self.storage.change_task_list(task_list)

            # Log info
            self.logger.info("The user with login: {} was added to the task list with id: {}."
                             .format(user_login, task_list.id))

            return task_list
        self.logger.info("The user with login: {} doesn't exist in database."
                         .format(user_login))

    @check_permissions
    @write_log
    def remove_user_from_list(self, task_list, current_user, user_login):
        """ Remove user from task list

        Keyword argument:
            task_list -- task list to change
            current_user -- user who change the task list
            user_login -- user login to remove

        Raises:
            NotExistingObjectInListError -- if user doesn't exist in list of users
            TypeError -- if task list isn't TaskList object
            TaskListWithOneUserError -- if list of users has only one user
        """

        if not isinstance(task_list, TaskList):
            raise TypeError("Task list isn't TaskList object.")

        # Check absence user_login in list of users
        if user_login not in task_list.users:
            raise exceptions.NotExistingObjectInListError(User, TaskList)

        # If list of users has only one user, raise error
        if task_list.users.__len__() == 1:
            raise exceptions.TaskListWithOneUserError(user_login,task_list.name)

        # Remove user from list of users
        task_list.remove_user(user_login)

        # Change task list in storage
        self.storage.change_task_list(task_list)

        # Log info
        self.logger.info(
            "The user with login: {} was removed from the task list with id: {}."
            .format(user_login, task_list.id))

        return task_list

    def get_valid_task_list_priority(self, priority):
        """ Check task list priority to get valid priority

        Keywords arguments:
            priority -- task list priority to check

        Raises:
            NotValidParameterError -- if priority is incorrect
        """

        # Return value if it's Priority
        if isinstance(priority, Priority):
            return priority

        # Return value if priority is number
        elif isinstance(priority, int):
            if 0 <= priority <= 2:
                return Priority(priority)

        elif isinstance(priority, str):

            try:
                # Return value if priority is str that become number
                priority_number = int(priority)
                if 0 <= priority_number <= 2:
                    return Priority(priority_number)

            except ValueError:
                # Return value if priority is str
                priority = priority.upper()

                try:
                    return Priority[priority]

                except KeyError:
                    pass

        raise exceptions.NotValidParameterError(priority, "priority")

    def get_list_tasks(self, task_list_id):
        """ Return all task list tasks

        Keyword arguments:
            task_list_id -- task list id to get tasks
        """

        task_list_tasks = []
        tasks = self.storage.get_tasks()

        for task in tasks:
            # If task in the required task_list, then add it to list
            if task.task_list_id == task_list_id:
                task_list_tasks.append(task)

        return task_list_tasks

    def get_list_unfinished_tasks(self, task_list_id):
        """ Return all task list tasks

        Keyword arguments:
            task_list_id -- task list id to get tasks
        """

        task_list_tasks = []
        tasks = self.storage.get_tasks()

        for task in tasks:
            # If task in the required task_list, then add it to list
            if (str(task.task_list_id) == str(task_list_id) and 0 <= task.status.value <= 1
                    and task.parent is None):
                task_list_tasks.append(task)

        return task_list_tasks

    def get_list_finished_tasks(self, task_list_id):
        """ Return all task list tasks

        Keyword arguments:
            task_list_id -- task list id to get tasks
        """

        task_list_tasks = []
        tasks = self.storage.get_tasks()

        for task in tasks:
            # If task in the required task_list, then add it to list
            if (task.task_list_id == task_list_id and task.status.value == 2
                    and task.parent is None):
                task_list_tasks.append(task)

        return task_list_tasks

    def get_list_failed_tasks(self, task_list_id):
        """ Return all task list tasks

        Keyword arguments:
            task_list_id -- task list id to get tasks
        """

        task_list_tasks = []
        tasks = self.storage.get_tasks()

        for task in tasks:
            # If task in the required task_list, then add it to list
            if (task.task_list_id == task_list_id and task.status.value == 3
                    and task.parent is None):
                task_list_tasks.append(task)

        return task_list_tasks

    def show_full_list_info(self, task_list):
        """ Show full information about task list

        Keyword_arguments:
            task_list -- task list object
        """

        print('\n--------------------------------------')

        print("ID: {}\nName: {}\nPriority: {}"
              .format(task_list.id, task_list.name, task_list.priority.name))

        from lib_task_tracker.entities.user import user_manager
        user_mng = user_manager.UserManager(self.storage)
        print("Users: ")
        for user_login in task_list.users:
            user = self.storage.get_user_by_login(user_login)
            user_mng.show_short_user_info(user)

        from lib_task_tracker.entities.task import task_manager
        task_mng = task_manager.TaskManager(self.storage)

        # Get all task list tasks
        tasks = self.get_list_tasks(task_list.id)

        # Print all task list tasks
        if tasks is not None:
            print("Tasks: ")
            tasks.sort(key=lambda t:t.priority.value)
            for task in tasks:
                task_mng.show_short_task_info(task)
        print('--------------------------------------\n')

    def show_short_list_info(self, task_list):
        """ Show short information about task list

        Keyword_arguments:
            task_list -- task list object
        """

        print("\tid: {}\n\t name: {}\n".format(task_list.id, task_list.name))