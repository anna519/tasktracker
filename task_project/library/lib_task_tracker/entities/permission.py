""" This module includes the decorator that checks the user permissions"""


from functools import wraps


def check_permissions(func):
    @wraps(func)
    def wrap(*args, **kwargs):

        from lib_task_tracker.entities.task_list import task_list
        from lib_task_tracker.entities.task import task
        from lib_task_tracker.entities.task.task_manager import TaskManager

        obj = args[1]
        current_user = args[2]

        permission = False

        task_mng = TaskManager(args[0].storage)
        if isinstance(obj, task_list.TaskList):
            if current_user.login in obj.users:
                permission = True
        elif isinstance(obj, task.Task):
            obj_task_list = task_mng.get_task_list_of_task(obj)
            if current_user.login in obj_task_list.users:
                permission = True
        else:
            raise TypeError()
        if permission:
            return func(*args, **kwargs)
        else:
            raise PermissionError(current_user.login)

    return wrap