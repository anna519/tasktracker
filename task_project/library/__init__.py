""" This library helps create and manage tasks

    The main entities that used in this library are User, Task, TaskList:

    --- Task ---
    Task entity is a task that user wants to complete. The task has a name. Also it has
    priority (by default -- AVERAGE), deadline, dependent task, status. Task can be
    subtask for other task.
    If task is subtask for other task it means if main task is completed than subtasks is done too.
    if task is dependent task for main task it means dependent task can't be change your status
    value before main task is completed.

    --- TaskList ---
    TaskList entity is a list that unites tasks. TaskList has such info as name,
    is_standart, priority and users who have access to tasklist.

    --- User ---
    User entity is a user who can has tasklists and complete tasks. User necessery has
    unique email, unique login and password. He also has first name and last name.

    Classes UserManager, ListManager, TaskManager exists to manage User, TaskList and Task in the database.
    The data will be stored in the storage. By default the library uses json.

    --- UserManager ---
    UserManager is a class that helps manage User entity with database.
    It helps create, update, remove user.

    --- ListManager ---
    ListManager is a class that helps manage List entity with database.
    It helps create, update, remove task list.

    --- TaskManager ---
    Task Manager is a class that helps manage Task entity with database.
    It helps create, update, remove task.

    The library has own exceptions and logging system. The logger writes on the DEBUG
    level when the functions started and finished, on the ERROR and CRITICAL level
    when error was occupied, on the INFO level some information about action.

    Example:

        > import os
        > import lib_task_tracker.entities.user.user_manager
        > import lib_task_tracker.entities.task.task_manager

        > path = os.path.expanduser('~/.TaskTracker/Data')

        > user_manager = lib_task_tracker.entities.user.user_manager.UserManager(storage_path=path)
        > user = user_manager.create_user('email', 'login', 'password')
        > task = task_manager.create_task('Task', user)


    Packages:
    library -- main project library

    Module:
    setup -- setup to install the library
    README -- file for first meeting with library
"""