#TaskManager
The application allows you to track tasks, create different task list and
don't forget to do something important.

### Prerequisites
To use the app install jsonpickle:
`pip3 install jsonpicke`

### Getting started

Clone the project from repository:
> git clone https://bitbucket.org/anna519/tasktracker.git

Install the app library from the cloned project folder:
`cd task_project/library`

`python3 setup.py install`

Install the console app:
`cd task_project/console_app`
`python3 setup.py install`

After the installation you can use the console app with the command 'TaskTracker':

`TaskTracker list create "New Task list"`

`2018-09-01 09:57:36,798 | INFO : The  task list with id: 4f1f2f48-adb4-11e8-9736-80a5899b7857 was added.`

`List was successfully added.`

### Instuction

If you open the app at first time you must create user profile with command:
`TaskTracker user create 'email' 'login' 'password'`

Then you should authenticate yourself with command:
`TaskTracker user authenticate 'login' 'password'`

And now you can use all TaskTracker features.

At the end you can log out with command:
`TaskTracker user 'log off'`

### Author
* Hanna Nikitsinskaya
