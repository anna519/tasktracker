from setuptools import setup, find_packages


setup(
    name="Task_tracker_console",
    version="1.2.1",
    description='The command line app to manage tasks.',
    author='Hanna Nikitsinskaya',
    url='https://bitbucket.org/anna519/tasktracker',
    packages=find_packages(),
    install_requires = ["Task_tracker_lib"],
    entry_points={
        'console_scripts':
            ['TaskTracker=console_task_tracker.main:main']
    },
    include_package_data=True
)