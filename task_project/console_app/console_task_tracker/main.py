import os
import logging
from datetime import datetime
from console_task_tracker.parser_commands.parser import  get_parser
from console_task_tracker.parser_commands.handlers.commands_handler import CommandsHandler
from lib_task_tracker import exceptions
from lib_task_tracker.logger import get_logger
from console_task_tracker.settings import get_logging_level, create_app_directory, \
    get_storage_path


APP_DATA_PATH = '~/.TaskTracker/Data'
APP_SETTINGS_PATH = '~/.TaskTracker/settings.ini'
APP_LOGGER_PATH = '~/.TaskTracker/Logger'


def set_default_settings(app_data_dir=None, settings_file_path=None):
    if app_data_dir is None:
        app_data_dir = APP_DATA_PATH
    create_app_directory(app_data_dir)

    if settings_file_path is None:
        settings_file_path = APP_SETTINGS_PATH

    settings_file = os.path.expanduser(settings_file_path)

    try:
        _ = get_storage_path(settings_file)

    except exceptions.SettingsNotFoundError:
        settings = os.path.expanduser(settings_file)
        with open(settings, 'w'):
            pass
        print("The setttings wasn't found. A new empty settings file was created")
    except (KeyError, exceptions.PathNotFoundError):
        pass


def set_logger(log_dir=None, log_file_name=None):
    if log_dir is None:
        log_dir = APP_LOGGER_PATH

    log_dir = os.path.expanduser(log_dir)

    if not os.path.exists(log_dir):
        os.mkdir(log_dir)
    if log_file_name is None:
        log_file_name = str(datetime.now().date())
    log_file = os.path.join(log_dir, log_file_name)
    logger = get_logger()

    level = get_logging_level()
    logger.setLevel(level)

    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)

    console_formatter = logging.Formatter('%(asctime)s | %(levelname)s : %(message)s',
                                          datefmt='%d-%m-%y %H:%M:%S')
    console_handler.setFormatter(console_formatter)

    file_handler = logging.FileHandler(log_file)
    file_handler.setLevel(logging.DEBUG)

    file_formatter = logging.Formatter('%(asctime)s | module: %(name)s ; function : '
                                       '%(funcName)s | %(levelname)s : %(message)s',
                                       datefmt='%d-%m-%y %H:%M:%S')
    file_handler.setFormatter(file_formatter)

    logger.addHandler(console_handler)
    logger.addHandler(file_handler)


def main():
    set_default_settings()
    set_logger()

    parser = get_parser()
    args_dict = vars(parser.parse_args())

    handler = CommandsHandler(args_dict)
    handler.handle_actions()


if __name__ == "__main__":
    main()