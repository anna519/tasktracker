import os
import configparser
import uuid
from lib_task_tracker import exceptions
from lib_task_tracker.logger import get_logger

APP_DIR_PATH = '~/.TaskTracker'
APP_SETTINGS_PATH = '~/.TaskTracker/settings.ini'
SETTINGS_NAME = 'settings.ini'


def add_current_user_id(user_id):
    """ Add current user id to the settings.

    Keyword argument:
        user_id -- current user id
    """

    app_path = create_app_directory()

    settings_path = os.path.join(app_path, SETTINGS_NAME)
    config = configparser.RawConfigParser()

    config.read(settings_path)
    config['CURRENT'] = {'User': str(user_id)}

    with open(settings_path, 'w') as settings_file:
        config.write(settings_file)


def get_current_user_id():
    """ Return current user id

    Raises:
        KeyError -- if [CURRENT] User doesn't exist in the settings
        SettingsNotFoundError -- if the settings file doesn't exist
        ValueError -- if the current user id from the settings is incorrect
    """

    settings_path = os.path.expanduser(APP_SETTINGS_PATH)

    if not os.path.isfile(settings_path):
        raise exceptions.SettingsNotFoundError()

    config = configparser.ConfigParser()
    config.read(settings_path)

    if 'CURRENT' in config and 'User' in config['CURRENT']:
        current_user_id = config['CURRENT']['User']
        if current_user_id != "None":
            try:
                return uuid.UUID(current_user_id)
            except (ValueError, TypeError):
                raise ValueError("Not valid current user id in the settings")
        else:
            raise KeyError("[CURRENT] User doesn't exist in the settings")      


def create_app_directory(app_dir_path=APP_DIR_PATH):
    """ Create app directory in the system if it doesn't exist."""

    project_path = os.path.expanduser(app_dir_path)

    if not os.path.exists(project_path):
        os.mkdir(project_path)

    return project_path


def add_storage_path(storage_path=APP_DIR_PATH):
    """ Add the data storage path to the settings configuration

    Keyword arguments:
        storage_path -- path to store the data (APP_DIR_PATH)

    Raises:
        PathNotFoundError -- if the storage path is incorrect
    """

    app_path = create_app_directory()

    settings_path = os.path.join(app_path, SETTINGS_NAME)
    storage_path = os.path.expanduser(storage_path)

    if not os.path.exists(storage_path):
        raise exceptions.PathNotFoundError(storage_path)

    config = configparser.RawConfigParser()
    config.read(settings_path)

    config["DEFAULT"] = {'DataPath': storage_path}

    with open(settings_path, 'w') as settings_file:
        config.write(settings_file)


def get_storage_path(settings_path=None):
    """ Return the data storage path

    Raises:
        KeyError -- if [DEFAULT] DataPath doesn't exist in the settings
        PathNotFound -- if the default data path from the settings is incorrect
        SettingsNotFound -- if the settings file doesn't exist
    """

    if settings_path is None:
        settings_path = os.path.expanduser(APP_SETTINGS_PATH)

    if not os.path.exists(settings_path):
        raise exceptions.SettingsNotFoundError()

    config = configparser.ConfigParser()

    config.read(settings_path)

    if 'DEFAULT' in config and 'DataPath' in config['DEFAULT']:
        default_data_path = config['DEFAULT']['DataPath']
        if os.path.exists(default_data_path):
            return default_data_path
        else:
            raise exceptions.PathNotFoundError(settings_path)

    else:
        raise KeyError("[DEFAULT] DataPath doesn't exist in the settings.")


def get_logging_level(settings_path=APP_SETTINGS_PATH):
    """ Return the level of the logger

    Raises:
        KeyError -- if [logger_taskTracker] level doesn't exist in the settings
        NotValidParameterError -- if the logger level from the settings is incorrect
        SettingsNotFound -- if the settings file doesn't exist
    """

    settings_path = os.path.expanduser(settings_path)

    if not os.path.exists(settings_path):
        raise exceptions.SettingsNotFoundError()

    config = configparser.ConfigParser()

    config.read(settings_path)
    if 'LOGGER' in config and 'level' in config['LOGGER']:
        level = config['LOGGER']['level']

        level = level.upper()
        levels = ['DEBUG', 'INFO', 'WARN', 'WARNING', 'ERROR', 'CRITICAL']

        if level not in levels:
            raise exceptions.NotValidParameterError(level, 'level')

        return level

    raise KeyError("[LOGGER] level doesn't exist in the settings")


def change_logger_level(level, settings_path=None):
    """ Change the level of the logger

    Raises:
        KeyError -- if [logger_taskTracker] level doesn't exist
        NotValidParameterError -- if the logger level is incorrect
        LoggerConfigNotFound -- if the logger config file doesn't exist
    """

    if settings_path is None:
        app_dir_path = create_app_directory()
        settings_path = os.path.join(app_dir_path, SETTINGS_NAME)

    level = level.upper()
    levels = ['DEBUG', 'INFO', 'WARN', 'WARNING', 'ERROR', 'CRITICAL']

    if level not in levels:
        raise exceptions.NotValidParameterError(level, 'level')

    logger = get_logger()
    logger.setLevel(level)

    config = configparser.RawConfigParser()
    config.read(settings_path)

    config['LOGGER'] = {'level': level}
    with open(settings_path, 'w') as settings_file:
        config.write(settings_file)
