from lib_task_tracker.storage.database_manager import DataBaseManager
from console_task_tracker.settings import (add_current_user_id,
                                                     get_current_user_id)
from lib_task_tracker.exceptions import ObjectNotFoundError


def get_current_user():
    """ Return the current user

    Raises:
        NotExistingObjectError -- if the user by id from the settings
                                  doesn't exist
        KeyError -- if current user id doesn't exist in the settings
        TypeError -- if the current user id from the settings is incorrect
    """

    try:
        user_id = get_current_user_id()

    except (KeyError, TypeError, ObjectNotFoundError):
        print("The current user id from the settings doesn't exist "
                      "in the database.")
        user_id = None
    return user_id


def authenticate_user(user_login, user_password, storage):
    """ Authenticate the user

    Keyword arguments:
        user_login -- user login
        user_password -- user password
    """

    try:
        user = DataBaseManager(storage).get_user_by_login(user_login)
        if user.password == user_password:

            print('The user with login: {} was successfully authenticated'.
                  format(user_login))
            add_current_user_id(user.id)
            return user
        else:
            print('Wrong password.')

    except ObjectNotFoundError:
        print("The user with such login doesn't exist in the database.")


def exit_from_user_profile():
    """ Exit from user profile

    Keyword_argument:
        user_login -- user login
    """

    user_id = get_current_user()

    add_current_user_id(None)
    if user_id is not None:
        print('The user with login {} log off.'.format(user_id))
