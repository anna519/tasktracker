from lib_task_tracker.entities.task_list import list_manager
from lib_task_tracker.entities.user.user_manager import UserManager
from lib_task_tracker import exceptions
from console_task_tracker.parser_commands.authentication import get_current_user


def handle_list_exception(func):
    def wrap(*args, **kwargs):
        user = args[0].user

        if user is None:
            print("There's no current user. Authenticate the user to "
                  "perform commands")
            return

        try:
            func(*args, **kwargs)
        except (exceptions.ObjectNotFoundError,
                exceptions.ExistingObjectInListError,
                exceptions.NotExistingObjectInListError,
                exceptions.ExistingTaskListByNameError,
                exceptions.NotValidParameterError,
                exceptions.StandartTaskListError,
                exceptions.TaskListWithOneUserError) as e:
            print(e)
        except PermissionError:
            print("The user hasn't permission.")

    return wrap


class ListCommandHandler:

    def __init__(self, args_dict, storage):
        self._dict = args_dict
        self.storage = storage
        self.list_mng = list_manager.ListManager(storage)
        user_mng = UserManager(self.storage)
        self.user = user_mng.storage.get_user_by_id(get_current_user())

    def handle_commands(self):
        command_name = self._dict['command_name']
        list_commands = {
            'create': self.handle_create_list,
            'add': self.handle_add,
            'change': self.handle_change_list,
            'delete': self.handle_delete,
            'show': self.handle_show_list
        }
        if command_name in list_commands:
            list_commands[command_name]()

    @handle_list_exception
    def handle_create_list(self):
        params = self._dict

        user = self.user
        new_list = self.list_mng.create_list(params['name'], user,
                                             params['priority'])

        if new_list is not None:
            print('\nList was successfully added.')

    @handle_list_exception
    def handle_add(self):
        params = self._dict

        user = self.user
        task_list = self.list_mng.storage.get_task_lists_by_id(params['id'])

        adding_objects = {
            'user': self.list_mng.add_user_to_list
        }

        for param in params:
            if param in adding_objects and params[param] is not None:
                adding_objects[param](task_list, user, params[param])

    @handle_list_exception
    def handle_change_list(self):
        params = self._dict

        user = self.user
        task_list = self.list_mng.storage.get_task_lists_by_id(params['id'])

        task_list = self.list_mng.change_task_list(task_list, user, params['name'],
                                                   params['priority'])

        self.list_mng.show_full_list_info(task_list)

    @handle_list_exception
    def handle_delete(self):
        params = self._dict

        user = self.user
        task_list = self.list_mng.storage.get_task_lists_by_id(params['id'])

        deletion_objects = {
            'user': self.list_mng.remove_user_from_list
        }

        param_deleted = False
        for param in params:
            if param in deletion_objects and params[param] is not None:
                deletion_objects[param](task_list,user, params[param])
                param_deleted = True

        if param_deleted:
            self.list_mng.show_full_list_info(task_list)
        else:
            self.list_mng.remove_list(task_list, user)

    @handle_list_exception
    def handle_show_list(self):
        params = self._dict

        if params['id'] is None:
            user_mng = UserManager(self.storage)
            user = self.user
            user_lists = user_mng.get_user_lists(user.login)
            user_lists.sort(key=lambda t: t.priority.value)
            for user_list in user_lists:
                self.list_mng.show_full_list_info(user_list)
        else:
            task_list = self.list_mng.storage.get_task_lists_by_id(params['id'])
            self.list_mng.show_full_list_info(task_list)
