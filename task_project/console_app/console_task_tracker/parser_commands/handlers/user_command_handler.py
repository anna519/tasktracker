from lib_task_tracker.entities.user.user_manager import UserManager
from console_task_tracker.settings import (add_current_user_id,
                                                            get_current_user_id)
from console_task_tracker.parser_commands import authentication as auth
from lib_task_tracker import exceptions


def handle_user_exception(func):
    def wrap(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except (exceptions.ExistingUserByEmailError,
                exceptions.ExistingUserByLoginError,
                exceptions.ObjectNotFoundError) as e:
            print(e)

    return wrap


class UserCommandHandler:

    def __init__(self, args_dict, storage):
        self._dict = args_dict
        self.storage = storage
        self.user_mng = UserManager(storage)

    def handle_commands(self):
        command_name = self._dict['command_name']
        user_commands = {
            'authenticate': self.handle_authentication,
            'log off': self.handle_log_off,
            'create': self.handle_create_user,
            'change': self.handle_change_user,
            'delete': self.handle_delete,
            'show': self.handle_show_user
        }
        if command_name in user_commands:
            user_commands[command_name]()

    @handle_user_exception
    def handle_create_user(self):
        params = self._dict

        new_user = self.user_mng.create_user(params['email'], params['login'],
                                             params['password'],
                                             params['first_name'],
                                             params['last_name'])
        self.user_mng.show_full_user_info(new_user)

    @handle_user_exception
    def handle_change_user(self):
        params = self._dict
        user = self.user_mng.storage.get_user_by_login(params['old-login'])

        user = self.user_mng.change_user(user, params['email'], params['login'],
                                         params['first_name'], params['last_name'],
                                         params['password'])
        print()
        self.user_mng.show_full_user_info(user)

    @handle_user_exception
    def handle_delete(self):
        params = self._dict
        user = self.user_mng.storage.get_user_by_login(params['login'])

        deletion_object = params['parameter_name']

        if deletion_object is not None:
            user = self.user_mng.change_user(user, {deletion_object: None})

            print()
            self.user_mng.show_full_user_info(user)
        else:
            cur_user_id = get_current_user_id()
            self.user_mng.remove_user(user)
            print('\nThe user was successfully removed.')

            if cur_user_id == user.id:
                add_current_user_id(None)

                print('\nThe deleted user was the current user of the app.')

    @handle_user_exception
    def handle_show_user(self):
        params = self._dict
        if (params['login'] is None and params['id'] is None and
                params['email'] is None):

            users = self.user_mng.storage.get_users()
            if users.__len__() == 0:
                print("There's no users in database.")
            else:
                for user in users:
                    self.user_mng.show_full_user_info(user)
            return

        user = None
        if params['id'] is not None:
            user = self.user_mng.storage.get_user_by_id(params['id'])

        elif params['login'] is not None:
            user = self.user_mng.storage.get_user_by_login(params['login'])

        elif params['email'] is not None:
            user = self.user_mng.storage.get_user_by_email(params['email'])

        if user is not None:
            self.user_mng.show_full_user_info(user)
        else:
            print('\nThere is no user with such parameter.')

    def handle_authentication(self):
        params = self._dict
        auth.authenticate_user(params['login'], params['password'], self.storage)

    def handle_log_off(self):
        auth.exit_from_user_profile()
