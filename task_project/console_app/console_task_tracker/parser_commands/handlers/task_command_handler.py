from lib_task_tracker.entities.task.task_manager import TaskManager
from lib_task_tracker.entities.user import user_manager
from console_task_tracker.parser_commands.authentication import get_current_user
from lib_task_tracker import exceptions


def handle_task_exception(func):
    def wrap(*args, **kwargs):
        user = args[0].user

        if user is None:
            print("There's no current user. Authenticate the user to "
                  "perform commands")
            return

        try:
            func(*args, **kwargs)
        except (exceptions.ObjectNotFoundError,
                exceptions.NotValidParameterError,
                exceptions.TaskIsBlockedError) as e:
            print(e)
        except PermissionError:
            print("The user hasn't permission.")
    return wrap


class TaskCommandHandler:

    def __init__(self, args_dict, storage):
        self._dict = args_dict
        self.storage = storage
        self.task_mng = TaskManager(storage)
        user_mng = user_manager.UserManager(self.storage)
        self.user = user_mng.storage.get_user_by_id(get_current_user())

    def handle_commands(self):
        command_name = self._dict['command_name']
        task_commands = {
            'create': self.handle_create_task,
            'add': self.handle_add,
            'change': self.handle_change_task,
            'delete': self.handle_delete,
            'show': self.handle_show_tasks
        }
        if command_name in task_commands:
            task_commands[command_name]()

    @handle_task_exception
    def handle_create_task(self):
        params = self._dict

        task_list_id = params['task_list']
        if task_list_id is not None:
            task_list = self.task_mng.storage.get_task_lists_by_id(task_list_id)
        else:
            task_list = None
        user = self.user
        new_task = self.task_mng.create_task(params['name'], user, task_list,
                                             params['deadline'], params['priority'])

        print("\nTask was successfully added.")
        self.task_mng.show_full_task_info(new_task)

    @handle_task_exception
    def handle_add(self):
        params = self._dict

        task = self.task_mng.storage.get_task_by_id(params['id'])
        user = self.user

        adding_object = params['parameter_name']

        adding_objects = {
            'dependent': self.task_mng.add_dependent_task,
            'subtask': self.task_mng.add_sub_task
        }

        if adding_object in adding_objects:
            new_task = adding_objects[adding_object](task, user, params['name'],
                                          params['deadline'], params['priority'])
            if new_task:
                print("Task was added.")
            else:
                print("Task hasn't been added.")

    @handle_task_exception
    def handle_change_task(self):
        params = self._dict

        task = self.task_mng.storage.get_task_by_id(params['id'])
        user = self.user

        params = {param: value for param, value in params.items()
                  if value is not None}

        try:
            _ = params['parameter_name']
            month = 0
            day = 0
            hour = 0
            year = 0
            for param in params:
                try:
                    if param == 'year':
                        year += int(params[param])
                    if param == 'month':
                        month += int(params[param])
                    if param == 'day':
                        day += int(params[param])
                    if param == 'hour':
                        hour += int(params[param])
                except ValueError:
                    raise exceptions.NotValidParameterError(params[param], "period")
            self.task_mng.change_period(task, user, year, month, day, hour, params['number'])
        except KeyError:
            task = self.task_mng.change_task(task, user, params)

        self.task_mng.show_full_task_info(task)

    @handle_task_exception
    def handle_delete(self):
        params = self._dict

        main_task = self.task_mng.storage.get_task_by_id(params['id'])

        user = self.user

        deletion_object = params['parameter_name']

        if deletion_object is None:
            self.task_mng.remove_task(main_task, user)
            print("\nThe task was successfully removed.")
        else:
            deletion_objects = {
                'dependent': self.task_mng.remove_dependent_task,
                'subtask': self.task_mng.remove_subtask
            }

            if deletion_object in deletion_objects:
                second_task = self.task_mng.storage.get_task_by_id(
                    params['second-task-id'])
                is_delete = deletion_objects[deletion_object](main_task, user, second_task)
                if is_delete:
                    print("Link was removed.")
                else:
                    print("Link hasn't been removed.")
            elif deletion_object == 'deadline':
                main_task = self.task_mng.change_task(main_task, user,
                                                      {deletion_object: None})
                self.task_mng.show_full_task_info(main_task)

    @handle_task_exception
    def handle_show_tasks(self):
        params = self._dict

        if params['id'] is not None:
            task = self.task_mng.storage.get_task_by_id(params['id'])
            self.task_mng.show_full_task_info(task)
        elif params['filter'] == 'unfinished':
            user = self.user
            user_mng = user_manager.UserManager(self.storage)
            user_tasks = user_mng.get_user_unfinished_tasks(user.login)
            for user_task in user_tasks:
                self.task_mng.show_full_task_info(user_task)
        elif params['filter'] == 'finished':
            user = self.user
            user_mng = user_manager.UserManager(self.storage)
            user_tasks = user_mng.get_user_finished_tasks(user.login)
            user_tasks.sort(key=lambda t:t.priority.value)
            for user_task in user_tasks:
                self.task_mng.show_full_task_info(user_task)
        elif params['filter'] == 'failed':
            user = self.user
            user_mng = user_manager.UserManager(self.storage)
            user_tasks = user_mng.get_user_failed_tasks(user.login)
            user_tasks.sort(key=lambda t:t.priority.value)
            for user_task in user_tasks:
                self.task_mng.show_full_task_info(user_task)
        else:
            user = self.user
            user_mng = user_manager.UserManager(self.storage)
            user_tasks = user_mng.get_user_tasks(user.login)
            user_tasks.sort(key=lambda t:t.priority.value)
            for user_task in user_tasks:
                self.task_mng.show_full_task_info(user_task)
