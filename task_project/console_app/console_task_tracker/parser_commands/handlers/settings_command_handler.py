from lib_task_tracker import exceptions
from console_task_tracker import settings


class SettingsCommandsHandler:
    def __init__(self, args_dict):
        self._dict = args_dict

    def handle_commands(self):
        command_name = self._dict['command_name']
        settings_commands = {
            'add': self.handle_adding,
            'show': self.handle_view
        }
        if command_name in settings_commands:
            settings_commands[command_name]()

    def handle_adding(self):
        parameters = self._dict
        settings_params = {
            'data_path': settings.add_storage_path,
            'logging_level': settings.change_logger_level
        }

        for param in settings_params:
            if param in parameters and parameters[param] is not None:
                try:
                    settings_params[param](parameters[param])
                except exceptions.PathNotFoundError as e:
                    print(str(e))
                except (exceptions.LoggerConfigNotFoundError, KeyError,
                        exceptions.NotValidParameterError) as e:
                    print(str(e))

    def handle_view(self):
        view_objects = self._dict['parameter_name']
        settings_params = {
            'data-path': settings.get_storage_path,
            'logging-level': settings.get_logging_level,
            'cur-user': settings.get_current_user_id
        }

        if view_objects in settings_params:
            try:
                param = settings_params[view_objects]()
                print(param)
            except (exceptions.LoggerConfigNotFoundError, KeyError,
                    exceptions.NotValidParameterError) as e:
                print(str(e))