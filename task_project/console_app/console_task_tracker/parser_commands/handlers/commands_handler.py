from console_task_tracker.parser_commands.handlers.list_command_handler \
    import ListCommandHandler
from console_task_tracker.parser_commands.handlers.task_command_handler \
    import TaskCommandHandler
from console_task_tracker.parser_commands.handlers.user_command_handler \
    import UserCommandHandler
from console_task_tracker.parser_commands.handlers.settings_command_handler \
    import SettingsCommandsHandler
from console_task_tracker.settings import get_storage_path
from lib_task_tracker.storage.database_manager import DataBaseManager
from lib_task_tracker.storage.json.json_manager import JsonManager
from lib_task_tracker.exceptions import PathNotFoundError


class CommandsHandler:
    def __init__(self, args_dict):
        self.params = args_dict

    def handle_actions(self):
        object_name = self.params['object']
        objects = {
            'task': TaskCommandHandler,
            'user': UserCommandHandler,
            'list': ListCommandHandler,
        }
        if object_name in objects:
            try:
                storage_path = get_storage_path()
                storage = DataBaseManager(JsonManager(storage_path))
                objects[object_name](self.params, storage).handle_commands()
            except (PathNotFoundError, KeyError) as e:
                print(e)
        elif object_name == 'settings':
            SettingsCommandsHandler(self.params).handle_commands()
