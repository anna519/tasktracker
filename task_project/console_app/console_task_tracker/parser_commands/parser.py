import sys
from argparse import ArgumentParser


class TaskManagerParser(ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)


def get_parser():
    parser = TaskManagerParser(prog="TaskTracker",
                            description="""The application allows you to track
                            tasks, create different task list and don't forget
                            to do something important.""")

    subparsers = parser.add_subparsers(help='''The object with which you want
                                             to perform an action.''',
                                       dest='object')
    subparsers.required = True
    initialize_task_parser(subparsers)
    initialize_list_parser(subparsers)
    initialize_user_parser(subparsers)
    initialize_settings_parser(subparsers)

    return parser


# region Task parser
def initialize_task_parser(subparsers):
    task_parser = subparsers.add_parser('task',
                                        help='Performs command with tasks.')

    task_subparsers = task_parser.add_subparsers(help='The commands that can be performed.',
                                                 dest='command_name')

    task_subparsers.required = True

    initialize_add_task_parser(task_subparsers)
    initialize_change_task_parser(task_subparsers)
    initialize_create_task_parser(task_subparsers)
    initialize_delete_task_parser(task_subparsers)
    initialize_show_task_parser(task_subparsers)


def initialize_create_task_parser(task_subparsers):
    create_task_parser = task_subparsers.add_parser('create',
                                                    help='Create a task.')

    create_task_parser.add_argument('name', type=str, help='Task name.')

    create_task_parser.add_argument('-l', '--task-list', type=str,
                                    help='Task list id.')
    create_task_parser.add_argument('-d', '--deadline', type=str,
                                    help='Task deadline in H-M d-m-Y or d-m-Y.')
    create_task_parser.add_argument('-pr', '--priority', type=str,
                                    help='''Task priority(0 - HIGH, 
                                    1 - AVERAGE, 2 - LOW).''')


def initialize_add_task_parser(task_subparsers):
    add_task_parser = task_subparsers.add_parser('add',
                                                 help='Add some links between tasks.')
    add_task_parser.add_argument('id', type=str, help='Task id.')

    add_task_subparsers = add_task_parser.add_subparsers(help='The objects that '
                                                              'can be added.',
                                                         dest='parameter_name')
    sub_task_parser = add_task_subparsers.add_parser('subtask', help='Create the subtask.')

    dependent_task_parser = add_task_subparsers.add_parser('dependent', help='Create dependent task.')

    initialize_task_parser_arguments(sub_task_parser)
    initialize_task_parser_arguments(dependent_task_parser)


def initialize_task_parser_arguments(task_parser):
    task_parser.add_argument('name', type=str, help='Task name.')

    task_parser.add_argument('-d', '--deadline', type=str,
                                    help='Task deadline in H-M d-m-Y or d-m-Y.')
    task_parser.add_argument('-pr', '--priority', type=str,
                                    help='''Task priority(0 - HIGH, 
                                    1 - AVERAGE, 2 - LOW).''')


def initialize_change_task_parser(task_subparsers):
    change_task_parser = task_subparsers.add_parser('change',
                                                    help='Change some task fields.')
    change_task_parser.add_argument('id', type=str, help='Task id.')
    change_task_parser.add_argument('-d', '--deadline', type=str,
                                    help='New task deadline in H-M d-m-Y or d-m-Y.')
    change_task_parser.add_argument('-pr', '--priority', type=str,
                                    help=''''New task priority(0 - HIGH, 
                                    1 - AVERAGE, 2 - LOW).''')
    change_task_parser.add_argument('-n', '--name', type=str,
                                    help='New task name.')
    change_task_parser.add_argument('-s', '--status', type=str,
                                    help='New task status(0 - NOT_STARTED, 1 - STARTED, 2 - DONE, 3 - FAILED).')

    change_task_subparsers = change_task_parser.add_subparsers(help='The parameter that can be changed',
                                                               dest='parameter_name')
    period_parser = change_task_subparsers.add_parser('period', help='Task period (')
    initialize_period_parser(period_parser)


def initialize_period_parser(period_parser):
    period_parser.add_argument('-min', '--minute', type=str, help='Task periodicity in minutes')
    period_parser.add_argument('-hr', '--hour', type=str, help='Task periodicity in hours')
    period_parser.add_argument('-d', '--day', type=str, help='Task periodicity in days')
    period_parser.add_argument('-m', '--month', type=str, help='Task periodicity in months')
    period_parser.add_argument('number', type=str, help='Number of periodic tasks to create')


def initialize_delete_task_parser(task_subparsers):
    delete_task_parser = task_subparsers.add_parser('delete',
                                                    help='Delete some task fields.')
    delete_task_parser.add_argument('id', type=str, help='Task id.')
    delete_task_subparsers = delete_task_parser.add_subparsers(help='The parameter that can be deleted',
                                                               dest='parameter_name')
    sub_task_parser = delete_task_subparsers.add_parser('subtask', help='Subtask id.')
    sub_task_parser.add_argument('second-task-id', type=str, help='Subtask id to delete')

    dependent_parser = delete_task_subparsers.add_parser('dependent', help='Dependent task id.')
    dependent_parser.add_argument('second-task-id', type=str,
                                 help='Dependent task id to delete')

    delete_task_subparsers.add_parser('deadline', help='Delete task deadline')
    delete_task_subparsers.add_parser('period', help='Delete task period')


def initialize_show_task_parser(task_subparsers):
    show_task_parser = task_subparsers.add_parser('show',
                                                  help='Show info about task.')
    show_task_parser.add_argument('--id', type=str, help='Task id.')
    show_task_parser.add_argument('--filter', type=str, help='Filter to show (unfinished, finished, failed).')
# endregion


# Task list parser
def initialize_list_parser(subparsers):
    list_parser = subparsers.add_parser('list',
                                        help='Performs command with task list.')

    list_subparsers = list_parser.add_subparsers(dest='command_name')
    list_subparsers.required = True

    initialize_create_list_parser(list_subparsers)
    initialize_add_list_parser(list_subparsers)
    initialize_change_list_parser(list_subparsers)
    initialize_delete_list_parser(list_subparsers)
    initialize_show_list_parser(list_subparsers)


def initialize_create_list_parser(list_subparsers):
    create_list_parser = list_subparsers.add_parser('create',
                                                    help='Create a task list.')

    create_list_parser.add_argument('name', type=str, help='Task list name.')
    create_list_parser.add_argument('-pr', '--priority', type=str,
                                    help='''Task priority(0 - HIGH, 
                                    1 - AVERAGE, 2 - LOW).''')


def initialize_add_list_parser(list_subparsers):
    add_list_parser = list_subparsers.add_parser('add',
                                                 help='Add some task list fields.')
    add_list_parser.add_argument('id', type=str, help='Task list id.')
    add_list_parser.add_argument('-u', '--user', type=str, help='User login.')


def initialize_change_list_parser(list_subparsers):
    change_list_parser = list_subparsers.add_parser('change',
                                                    help='''Change some task 
                                                    list fields.''')
    change_list_parser.add_argument('id', type=str, help='The id of the task list to change.')
    change_list_parser.add_argument('-pr', '--priority', type=str,
                                    help=''''New task priority(0 - HIGH, 
                                    1 - AVERAGE, 2 - LOW).''')
    change_list_parser.add_argument('-n', '--name', type=str,
                                    help='New task list name.')


def initialize_delete_list_parser(list_subparsers):
    delete_list_parser = list_subparsers.add_parser("delete",
                                                    help='Delete some task fields.')
    delete_list_parser.add_argument('id', type=str, help='Task id.')
    delete_list_parser.add_argument('-u', '--user', type=str, help='User login to '
                                                                   'removed from task list.')


def initialize_show_list_parser(list_subparsers):
    show_list_parser = list_subparsers.add_parser("show",
                                                  help='Show info about task list if '
                                                       'the task list id is written '
                                                       'or all user task list.')
    show_list_parser.add_argument('--id', type=str, help='Task list id.')
# endregion


# region User parser
def initialize_user_parser(subparsers):
    user_parser = subparsers.add_parser('user',
                                        help='Performs command with users.')

    user_subparsers = user_parser.add_subparsers(dest='command_name')

    user_subparsers.required = True

    initialize_create_user_parser(user_subparsers)
    initialize_change_user_parser(user_subparsers)
    initialize_delete_user_parser(user_subparsers)
    initialize_show_user_parser(user_subparsers)
    initialize_user_authentication_parser(user_subparsers)
    initialize_user_log_off_parser(user_subparsers)


def initialize_create_user_parser(user_subparsers):
    create_user_parser = user_subparsers.add_parser('create',
                                                    help='Create a user.')

    create_user_parser.add_argument('email', type=str, help='User email.')
    create_user_parser.add_argument('login', type=str,
                                    help='User login.')
    create_user_parser.add_argument('password', type=str,
                                    help='User password.')
    create_user_parser.add_argument('--first-name', type=str,
                                    help='User first name.')
    create_user_parser.add_argument('--last-name', type=str,
                                    help='User second name.')


def initialize_change_user_parser(user_subparsers):
    change_user_parser = user_subparsers.add_parser('change',
                                                    help='Change some user fields.')
    change_user_parser.add_argument('old-login', type=str, help='User login')

    change_user_parser.add_argument('-e', '--email', type=str, help='New user email.')

    change_user_parser.add_argument('-l', '--login', type=str,
                                    help='New user login.')
    change_user_parser.add_argument('-p', '--password', type=str,
                                    help='New user password.')
    change_user_parser.add_argument('--first-name', type=str,
                                    help='New user first name.')
    change_user_parser.add_argument('--last-name', type=str,
                                    help='New user last name.')


def initialize_delete_user_parser(user_subparsers):
    delete_user_parser = user_subparsers.add_parser("delete",
                                                    help='Delete user by login.')
    delete_user_parser.add_argument('login', type=str,
                                    help='User login.')
    delete_user_subparsers = delete_user_parser.add_subparsers(help='The parameter that can be removed.',
                                                               dest='parameter_name')
    delete_user_subparsers.add_parser('first_name', help='User first name')
    delete_user_subparsers.add_parser('last_name', help='User last name')


def initialize_show_user_parser(user_subparsers):
    show_user_parser = user_subparsers.add_parser('show',
                                                  help='Show info about user if the user id, '
                                                       'login or email is written or show '
                                                       'info about all users.')
    group_parser = show_user_parser.add_mutually_exclusive_group()
    group_parser.add_argument('-i', '--id', type=str, help='User id.')
    group_parser.add_argument('-e', '--email', type=str, help='User email.')
    group_parser.add_argument('-l', '--login', type=str, help='User login.')


def initialize_user_authentication_parser(user_subparsers):
    user_parser = user_subparsers.add_parser('authenticate',
                                             help='Authenticate the user.')
    user_parser.add_argument('login', type=str, help='User login')
    user_parser.add_argument('password', type=str, help='User password')


def initialize_user_log_off_parser(user_subparsers):
    user_parser = user_subparsers.add_parser('log off',
                                             help='Log off the user')
# endregion


# region The settings parser
def initialize_settings_parser(subparsers):
    settings_parser = subparsers.add_parser('settings',
                                            help='Perform commands with the settings')
    settings_subparsers = settings_parser.add_subparsers(help='The commands that'
                                                              ' can be performed',
                                                         dest='command_name')
    initialize_adding_settings_parser(settings_subparsers)
    initialize_view_settings_parser(settings_subparsers)


def initialize_adding_settings_parser(settings_subparsers):
    adding_settings_parser = settings_subparsers.add_parser('add',
                                                            help='Add the params to the settings')
    adding_settings_parser.add_argument('-p', '--data-path', type=str,
                                        help='Sets the default data storage path.')
    adding_settings_parser.add_argument('-l', '--logging-level', type=str,
                                        help='Sets the logging level.')


def initialize_view_settings_parser(settings_subparsers):
    view_settings_parser = settings_subparsers.add_parser('show',
                                                          help='Show the settings params')

    view_settings_subparsers = view_settings_parser.add_subparsers(help='The parameters '
                                                                        'that can be shown',
                                                                   dest='parameter_name')
    view_settings_subparsers.add_parser('data-path',
                                          help='Show the default data storage path.')
    view_settings_subparsers.add_parser('logging-level',
                                          help='Show the logging level.')
    view_settings_subparsers.add_parser('cur-user',
                                        help='Show the current user id.')
# endregion
