# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import pytz
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from django.contrib import auth
from django.contrib.auth import login as auth_login
from task_tracker.forms import UserRegistrationForm, UserEditForm, TaskForm, TaskListForm
from task_tracker.models import User, TaskList, Task
from lib_task_tracker.entities.user.user_manager import UserManager
from lib_task_tracker.entities.task.task_manager import TaskManager
from lib_task_tracker.entities.task_list.list_manager import ListManager
from lib_task_tracker.storage.database_manager import DataBaseManager
from lib_task_tracker.storage.django.django_manager import DjangoManager



def get_storage():
    return DataBaseManager(DjangoManager())


def get_user_manager():
    storage = get_storage()
    return UserManager(storage)


def get_task_manager():
    storage = get_storage()
    return TaskManager(storage)


def get_list_manager():
    storage = get_storage()
    return ListManager(storage)

@csrf_protect
def login(request):
    args = {}
    if request.POST:
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return redirect('/home')
        args['login_error'] = 'User not found'
    return render(request,'authorization/login.html', args)


@csrf_protect
@login_required(login_url='/login')
def logout(request):
    auth.logout(request)
    return redirect('/login')


@csrf_protect
def register(request):
    args = {}
    if request.POST:
        new_user_form = UserRegistrationForm(request.POST)
        if new_user_form.is_valid():
            user = new_user_form.save()
            user_manager = get_user_manager()
            password = new_user_form.cleaned_data['password2']
            user_manager.create_user(user.email, user.username, password,
                                     user.first_name, user.last_name, user.id)
            auth_login(request, user)
            return redirect('/home')
    else:
        new_user_form = UserRegistrationForm()
    args['form'] = new_user_form
    return render(request, 'authorization/register.html', args)


@csrf_protect
@login_required(login_url='/login')
def main_page(request):
    args = {}
    args['username'] = auth.get_user(request).username
    user_manager = get_user_manager()
    task_lists = user_manager.get_user_lists(auth.get_user(request).username)
    args['task_lists'] = task_lists

    return render(request,'home.html', args)


@csrf_protect
@login_required(login_url='/login')
def add_task_list(request):
    if request.POST:
        form = TaskListForm(request.POST)
        if form.is_valid():
            task_list = form.save(commit=False)
            list_manager = get_list_manager()
            storage = get_storage()
            user = storage.get_user_by_login(auth.get_user(request).username)
            list_manager.create_list(task_list.task_list_name, user,
                                     priority=task_list.task_list_priority)
            return redirect('/home/')
    form = TaskListForm()
    args = {}
    args['form'] = form
    args['username'] = auth.get_user(request).username
    return render(request, 'task_list/add_task_list.html', args)


@csrf_protect
@login_required(login_url='/login')
def add_task(request, task_list_id):
    if request.POST:
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            task_manager = get_task_manager()
            storage = get_storage()
            deadline = None
            if task.task_deadline is not None and task.task_deadline != "":
                utc = pytz.UTC
                deadline = utc.localize(task.task_deadline)
            user = storage.get_user_by_login(auth.get_user(request).username)
            task_list = storage.get_task_lists_by_id(task_list_id)
            task_manager.create_task(task.task_name, user, task_list,
                                     deadline, task.task_priority, task.id)
            return redirect('/home/')
    form = TaskForm()
    args = {}
    args['form'] = form
    args['username'] = auth.get_user(request).username
    args['task_list_id'] = task_list_id
    return render(request, 'task/add_task.html', args)


@csrf_protect
@login_required(login_url='/login')
def view_user_profile(request):
    auth_user = request.user
    args = {}
    args['user'] = auth_user
    args['username'] = auth.get_user(request).username
    return render(request, 'user/profile.html', args)


@csrf_protect
@login_required(login_url='/login')
def edit_user_profile(request):
    if request.POST:
        form = UserEditForm(request.POST, isinstance=request.user)
        if form.is_valid():
            auth_user = form.save()
            user = User.objects.get(auth_user=auth_user)

            user_manager = get_user_manager()
            user_manager.change_user(user, )
    return render(request, 'user/profile.html', args)