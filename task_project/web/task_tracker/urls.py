from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^login/', views.login),
    url(r'^logout/', views.logout),
    url(r'^register/', views.register),
    url(r'^home/$', views.main_page),
    url(r'^addtasklist/$', views.add_task_list),
    url(r'^addtask/(?P<task_list_id>\d+)/$', views.add_task),
    url(r'^user/$', views.view_user_profile),
    url(r'^home/$', views.main_page),
]