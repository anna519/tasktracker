# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import django.utils.timezone
from django.db import models
from django.contrib.auth.models import User as AuthUser
from lib_task_tracker.entities.task.priority_enum import Priority as task_priority_enum
from lib_task_tracker.entities.task.status_enum import Status
from lib_task_tracker.entities.task_list.priority_enum import Priority as list_priority_enum


class User(models.Model):
    user_email = models.CharField(max_length=40)
    user_first_name = models.CharField(max_length=80, blank=True)
    user_last_name = models.CharField(max_length=80, blank=True)
    auth_user = models.OneToOneField(AuthUser, on_delete=models.CASCADE)

    def __str__(self):
        return self.auth_user.username


class TaskList(models.Model):
    task_list_name = models.CharField(max_length=20)

    list_priority_selection = [(pr.value, pr.name.replace("_", " ").title()) for pr in list_priority_enum]
    task_list_priority = models.SmallIntegerField(choices=list_priority_selection)

    task_list_users = models.ManyToManyField(User, null=True, blank=True)

    def __str__(self):
        return self.task_list_name


class Task(models.Model):
    task_name = models.CharField(max_length=200)

    task_tasklist = models.ForeignKey(TaskList, default='',
                                      on_delete=models.CASCADE)
    task_deadline = models.DateTimeField(blank=True, null=True)

    task_creation_date = models.DateTimeField(default=django.utils.timezone.now())

    task_status_selection = [(st.value, st.name.replace("_", " ").title()) for st in Status]
    task_status = models.SmallIntegerField(choices=task_status_selection)

    task_priority_selection = [(pr.value, pr.name.replace("_", " ").title()) for pr in task_priority_enum]
    task_priority = models.SmallIntegerField(choices=task_priority_selection)

    task_parent = models.ForeignKey("Task", related_name='subtask', blank=True,
                                    null=True, on_delete=models.CASCADE)
    task_depends_on = models.ForeignKey("Task", related_name='blocking',
                                        blank=True, null=True, on_delete=models.CASCADE)
