from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import User
from task_tracker.models import TaskList, Task


class UserRegistrationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', 'first_name',
                  'last_name')

    email = forms.CharField(max_length=254, required=True, widget=forms.EmailInput())
    first_name = forms.CharField(max_length=80, required=False, widget=forms.TextInput())
    last_name = forms.CharField(max_length=80, required=False, widget=forms.TextInput())


class UserEditForm(UserChangeForm):
    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'password')

    def __init__(self, *args, **kwargs):
        super(UserChangeForm, self).__init__(*args, **kwargs)
        del self.fields['password']


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ('task_name', 'task_status', 'task_deadline', 'task_priority')

        help_texts = {
            'task_deadline': "Deadline in 'YY-MM-dd h:m' format",
        }


class TaskListForm(forms.ModelForm):
    class Meta:
        model = TaskList
        fields = ('task_list_name', 'task_list_users', 'task_list_priority')
        