# TaskTracker 

### Аналоги


#### Списки дел
* Todoist 
> * Есть возможность создавать дела по определённым темам (проектам), которые уже имеют названия по умолчанию (работа, путешествия, семья и т.д.), но их можно также редактировать и создавать новые списки дел (список фильмов, учёба и т.д.).
> * Наличие горячих клавиш для удобной работы в приложении (добавить задачу, открыть поиск и т.д.).
> * Просмотр задач по датам либо на сегодня, либо на следующие 7 дней. (Является минусом, так как нет возможности визуализировать все свои дела на ближаший месяц, год и т.д.).
> * Наличие горячих клавиш, при помощи которых можно более удобно работать с несколькими задачами одновременно (помечать их как важные, завершённые и т.д.).
> * Просмотр завершённых задач по определённым спискам дел.
> * Возможность отмечать дела как важные.
> * Указывать приоритеты спискам дел.
> * Добавлять комментарии к делам.
> * Есть возможность устанавливать сроки выполнения задач.
> * Возможность задания уведомлений.
> * Возможность дублировать задачу.

* TickTick
> * Имеет те же возможности, что и предыдущие приложения. А также возможность просматривать задачи при помощи календаря (не только на 7 дней).

* Wunderlist
> * Имеет те же возможности, что и предыдущее приложение. За исключением того, что просматривать дела можно либо все, либо на сегодня.
> * Возможность указывать исполнителя задачи.
> * Возможность создавать подзадачи.
> * Возможность добавлять файлы к задачам.
> * Есть возможность добавлять описание задаче.
> * Есть возможность добавлять аудиосообщение к задачам.


 ___Таким образом___ _основной целью списков дел является создание отдельных задач. В pro версиях этих приложений есть возможность сотрудничать с группами людей и создавать общие задачи и назначать их определённым людям. Однако отстуствует возможность создавать связи между задачами._

#### Трекеры задач 
* Яндекс Трекер
> * При создании задачи, также можно написать её более развернутое описание, создавать различные связи, а также указывать важность задачи (незначительная, средняя и т.д.). 
> * У каждой задачи есть своя страница со всем её описанием (сроками выполнения, ответственных и др.).
> * Статистика задач.
> * Отчёт по временным затратам.
> * Возможность прикреплять файлы к задачам.
> * Возможность добавлять комментарии к задачам.
> * Возможность создания дашбордов.

___Таким образом___ _основной целью трекеров задач является работа с групповыми задачами. К минусам можно отнести отсутствие визуального представления сроков задач._

#### Календари online 

* Google Календарь и Yandex Календарь
> * Создание задач на определённую дату (при необходимости на конкретное время). 
> * Возможность создания события для группы людей с нахождением удобного времени для всех.
> * Возможность задания уведомлений.
> * Возможность указывать место для события.
> * Есть возможность открывать доступ для других пользователей.
> * Создание и импортирование новых календарей.

___Таким образом___ _основной целью календарей является удобная работа с различными событиями. Удобство в организации мероприятий с нахождением свободного времени для всех гостей. Также с их помощью можно достаточно наглядно представить свой план на ближайшие дни и месяцы. Однако в таком приложении невозможно работать с групповыми задачами и создавать иерархию задач._

---

### Сценарии использования 
* Создать список дел "Учёба". Добавить задачу "Выполнить лабораторную". Отметить как "важную". В процессе написания лабораторной отметить её как "начатую".

* Создать план, который один раз в месяц будет напоминать "Оплатить счета за квартиру".
 
 * Создать план заниматься спортом три раза в неделю. Настроить напоминания в начале, в середине и в конце недели. Отметить задачу как "проваленную" при невыполнении этого плана.

* Создать задачу "сходить в магазин". Случайно отметить  её как выполненную. Посмотреть выполненные задачи в данном списке дел. Исправить состояние как "не начатое". 

* Создать групповой список дел "Спектакль". 
	* Назначить ответственного. 
	* Создать иерархию дел: написать сценарий, подобрать музыку, сделать декорации, подготовить костюмы, найти актёров. 
	* Распределить дела по отдельным людям. 
	* Расставить зависимости, т.е. некоторые дела не могут быть начаты, пока не выполнены предыдущие (нельзя делать декорации и подбирать музыку, пока не написан сценарий).
	* Поставить крайний срок окончания работы (дата премьеры). 

---

### Планируемые возможности 

#### _Задача минимум_
	 
	 Создавать и редактировать списки дел.
	 Создавать и редактировать личные задачи в списках дел с пометками: важность, сроки выполнения, примечания и др..
	 Статусы выполнения задач: не начато, начато, выполнено, провалено и т.п..
	 Переводить задачи в категорию "проваленных" при истечении дедлайна.
	 Создание дубликата задачи.
	 Добавлять дедлайн для определённой задачи.
	 Изменять статус задачи: не начато, начато, завершено, провалено.
	 Возможность просмотреть выполненные задачи. 
	 Просмотр и создание задач на ближайшие 7 дней.
	 Возможность задания периодических планов.
	 
#### _Задача максимум_
	Наличие нескольких уровней приоритета задач и списков дел (важное, среднее, необязательное и т.п).
	Создание связей между задачами: зависит от ... , блокирует ..., без связи .
	Создавать несколько пользователей.
	Создавать групповые списки дел и задачи для нескольких пользователей.
	Назначать делам в групповых списках определённых исполнителей.
	Просмотр задач при помощи календаря.	
	Возможность оставлять комментарии к задачам.
	Возможность добавлять более развёрнутое описание задачи.
	Возможность прикреплять файлы к задачам.
	Отображение статистики (количество выполненных задач, не начатых, начатых, проваленных).
	Создание, редактирование и отображение сложных древовидных структур(подзадачи, подзадачи подзадач и т.д.).
	Настройка уведомлений о различных состояниях задач(предупреждение о приближении сроков).
	Настройка напоминаний для задач (планов).
	
---
### Логическая архитектура
* Имеется список пользователей, у каждого из которых имеется какой-то набор списков дел. Они могут иметь такую характеристику как приоритет. Также списки дел могут иметь нескольких пользователей, образуя тем самым групповой список задач. Списки дел состоят из определённых задач либо планов (задачи, которые повторяются с какой-то периодичностью). Каждая задача может иметь такие характеристики, как важность, связанность с другими задачами различным способом (быть зависимой или наоборот задавать эту зависимость,  либо не иметь каких либо связей), дедлайн, текущий статус, и т.п. . Также задачи могут образовывать древовидную структуру (быть вложенными, либо корневыми). Планы --- задачи, которые будут создаваться автоматически с указанным интервалом времени.

#### Сущности

* Задача 
	* Класс.
	* Обязательно должна иметь идентификатор, название, дату создания, приоритет и должна принадлежать какому-либо списку дел. Дополнительно может содержать описание задачи, исполнителя, дату выполнения, связи между задачами.
	* Можно создавать, изменять, удалять.
			
		* _Статус задачи_
		 	* Перечисление.
		 	* Возможные статусы: не начата, начата, завершена, просрочена.
 	
 		* _Приоритет задачи_
 			* Перечисление.
 			* Возможные приоритеты: важная, средняя, неважная.
	
* Задача-план.
	* Класс.
	* Должна иметь идентификатор, название, периодичность повторений и принадлежать списку дел.
	* Автоматически создается с заданной периодичностью.

* Список дел
 	* Класс
 	* Представляет собой список задач
 	* Имеет идентификатор, название, список задач, приоритет, пользователя (пользователей, если это групповой список).
 	* Изначально существует обязательный список дел "Входящие", который нельзя изменить и удалить. Все остальные списки дел можно создавать, изменять и удалять. 
 	* При удалении списка дел все задачи, содержащиеся в нём, удаляются.
 	
 		* _Приоритет списка дел_
 			* Перечисление.
 			* Возможные приоритеты: важный, средний, неважный.
 	 
* Пользователь 
	* Класс.
	* Обязательно должен иметь идентификатор, имя, фамилию, логин, пароль, список дел.
	* Может работать со своими списками дел, задачами, а также с групповыми списками дел.
	
* Группа
	* Класс.
	* Список пользователей.
	* Имеет идентификатор, список пользователей.
	* Каждый пользователь может создавать групповые списки дел, изменять их и удалять.
 	
 * Связь задача-задача
 
 	* Позволяет хранить связи между задачами.
 	* Имеет идентификатор одной задачи, идентификатор другой задачи и тип связи (блокирует, зависит).
 
	
#### Логика работы

* ___Добавление задач.___

	Чтобы добавить задачу, необходимо создать список дел, указав его приоритет. После чего в этом списке дел добавить задачу, указав необходимые характеристики. Также можно добавить задаче новую подзадачу либо указать первоначальную задачу как подзадачу какой-либо ранее созданной задачи.
	
* ___Выполнение задач.___

	Задача считается выполненной после того, как пользователь отметит задачу как "выполненную". Если задача является какой-либо частью древовидной структуры, то все её поддерево (задачи ниже по уровню вложенности, чем она сама), корнем которого она является, принимают также статус "выполненных" задач.

* ___Создание нового пользователя.___	
	
	При добавлении нового пользователя необходимо указать информацию о нём: логин, пароль, имя, фамилию и т.д. Всю информацию, кроме логина, можно потом отредактировать.
	
* ___Создание группы пользователей.___
	
	При создании группы пользователей можно добавить нескольких пользователей, указав их e-mail.

* ___Добавление задач для группы пользователей.___

	Один пользователь создаёт специальный групповой список задач, в котором должен указать других пользователей, которые будут иметь доступ к нему. Также можно указать главного человека для данного списка. При создании задач в групповых списках дел можно указывать ответственного человека для этого дела.
	
* ___Создание связей между задачами.___

	Задачи могут иметь следующие связи: 
	
	* зависит от ... .
	* блокирует ... .
	* без связи.
	
	Связи "зависит от ..." и "блокирует ..." являются взаимнообратными. То есть если _задача1_ "зависит от" _задачи2_, то _задача2_ "блокирует" _задачу1_. Эти связи означают, что _задача1_ не может быть выполнена, пока не будет выполнена _задача2_. 
		
	Связь "без связи" означает, что задача не задаёт никакую связь и сама зависимой не является.
	
	Чтобы задать задаче одну из этих связей необходимо указать её при создании, либо после создания отредактировать на нужную.
		
* ___Статусы задачи.___

	Задача имеет следующие статусы:
	
	* не начато;
	* начато;
	* выполнено;
	* провалено.
		
	При создании задачи первоначально её статус "не начато". После создания пользователь может редактировать статус задачи, т.е. отмечать её как "начатую" либо "выполненную".
	
	Если на момент истечения дедлайна (если он указан) задача не получила статус "выполненной" от пользователя, то она приобретает статус "проваленной". Убрать этот статус можно либо перенеся дедлайн, либо отметив задачу как "выполненную" (на случай, если пользователь забыл это сделать заранее).

* ___Уровни приоритета.___

	Задача может иметь три уровня приоритета: неважная, обычная, важная. Приоритет, который устанавливается по умолчанию, является "средний". Задачи в списках дел сортируются по важности (сначала идут "важные" задачи, затем средние по важности, и в конце "неважные").

---
### Этапы разработки

#### Базовая версия
В базовой версии приложения предоставляются возможности только для одного пользователя.

Этапы разработки | Предположительные сроки выполнения этих этапов
---| ---
Возможность создания списка дел. Добавление в них отдельных задач. | 10.04.2018
Возможность редактирования списков дел. Добавление характеристик для задач: статус, важность, дедлайн. Возможность редактировать задачи.| 23.04.2018
Возможность добавления планов. Создание и работа с древовидными структурами. Добавление приоритета для списка задач. Добавление связей между задачами. Создание одинаковых задач. Статистика задач.| 05.05.2018


#### Расширенная версия
В расширенной версии приложения появляется возможность пользоваться приложением нескольким пользователям.

Этапы разработки | Предположительные сроки выполнения этих этапов
---| ---
Добавлять новых пользователей. Удобное создание задач на ближайшие 7 дней. Создавать групповые списки дел. Добавлять подробное описание задачам. Добавлять комменатрии к задачам. Добавление календаря для удобства просмотра невыполненных задач. | 15.05.2018
Указывать ответственных в групповых списках дел. Добавлять дополнительные характеристики задачам в групповых списках (назначать ответственных на определённые дела). Возможность прикреплять файлы к задачам. Отправлять уведомления о приближении дедлайна. Отправлять напоминания. Отправлять уведомления для исполнителей конкретных задач. | 30.05.2018
Возможная доработка приложения. | 07.06.2018


---
### Статус
1. Отчёт завершён.
	Рассмотрены аналоги.
	Приведены планируемые возможности.
	Представлена логика работы приложения.
	Приведены этапы разработки и предположительные сроки их выполнения.
2. На этапе разработки находятся классы задач, пользотелей, списков дел.